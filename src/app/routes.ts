import { Homepage } from './pages/homepage';
import { Account } from './pages/account/account';
import { OrdersComponent } from './pages/account/orders/orders';
import { CurrentOrdersComponent } from './pages/account/orders/current-orders';
import { PastOrdersComponent } from './pages/account/orders/past-orders';
import { RSVPComponent } from './pages/account/rsvp/rsvp';
import { AcceptComponent } from './pages/account/rsvp/accept';
import { DeclineComponent } from './pages/account/rsvp/decline';
import { Routes } from '@angular/router';
import { PlaceOrderModule } from './pages/place-order/place-order';
import { PlaceOrderViewModule } from './pages/place-order/place-order-view';
import { AuthGuard } from './shared/_guard/auth.guard';
import { AdminViewModule } from './pages/admin/admin-view';
import { AdminViewOrderModule } from './pages/admin/admin-view-order';

export const MATERIAL_DOCS_ROUTES: Routes = [
  { path: '', component: Homepage, pathMatch: 'full' },
  { path: 'order', component: PlaceOrderViewModule, pathMatch: 'full'},
  {
    path: 'account',
    component: Account,
    //canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'orders', pathMatch: 'full' },
      {
        path: 'orders', component: OrdersComponent,
        children: [
          { path: '', redirectTo: 'current', pathMatch: 'full' },
          { path: 'current', component: CurrentOrdersComponent },
          { path: 'past', component: PastOrdersComponent }
        ]
      },
      {
        path: 'rsvp', component: RSVPComponent,
        children: [
          { path: '', redirectTo: 'accept', pathMatch: 'full' },
          { path: 'accept', component: AcceptComponent },
          { path: 'decline', component: DeclineComponent }
        ]
      },
    ]
  },
  {
    path: 'admin',
    children: [
      {
        path: 'list', component: AdminViewModule, pathMatch: 'full' //canActivate: [AuthGuard],
      },
      { path: 'view', component: AdminViewOrderModule, pathMatch: 'full' },
      { path: '', redirectTo: 'list', pathMatch: 'full' }
    ]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' },
  // {
  //   path: 'categories',
  //   component: ComponentSidenav,
  //   children: [
  //     {path: '', component: ComponentCategoryList},
  //     {path: ':id', component: ComponentList},
  //   ],
  // },
  // {
  //   path: 'components',
  //   component: ComponentSidenav,
  //   children: [
  //     {path: '', component: ComponentCategoryList},
  //     {path: 'component/:id', redirectTo: ':id', pathMatch: 'full'},
  //     {path: 'category/:id', redirectTo: '/categories/:id', pathMatch: 'full'},

  //   ],
  // },
  //{path: '**', redirectTo: ''},
  { path: 'api/categories', component: PlaceOrderModule },
];
