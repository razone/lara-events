import { Component, Directive, QueryList, ViewChildren, ElementRef, NgZone, Input, NgModule, OnInit, ViewChild, Inject, forwardRef, Output, EventEmitter } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SvgViewerModule } from '../../shared/svg-viewer/svg-viewer';
import { MatButtonModule } from '@angular/material';
import { RouterModule , Router, NavigationEnd} from '@angular/router';
import { ComponentPageTitle } from '../page-title/page-title';
import { NgControl } from '@angular/forms';
import { MatSelectModule, MatIconModule, MatRadioModule, MatDatepickerModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule, MatInputModule, MatIconRegistry } from '@angular/material';
import { FormsModule, FormArray, Validators, FormControl, FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Order, User, dateArray, addressArray } from '../place-order/order';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
//import {ImageUploadModule} from 'angular2-image-upload';
import { AlertService } from '../../shared/_services/alert.service';
import { DisableControlDirective } from '../../shared/_directives/DisableDirective';
import { UserService } from '../../shared/_services/user.service';
import { GooglePlaceModule } from "angular2-google-place"
import { SignUpDialogModule } from '../sign-up/sign-up';
import { PlaceOrderService } from '../place-order/place-order.service';
import { generalService } from '../homepage/general.service';
import { Observable } from 'rxjs/Observable';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { FileUploader, FileUploadModule, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import 'rxjs/add/operator/toPromise';
import { Cloudinary } from '@cloudinary/angular-4.x';
import { CustomValidators } from 'ng2-validation';
import { Category } from './category';
import { CategoryService } from './category.service';
import { FlexLayoutModule } from "@angular/flex-layout";
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'place-order-view',
  //directives: [GoogleplaceDirective],
  templateUrl: './place-order-view.html',
  styleUrls: ['place-order.scss']
})


export class PlaceOrderViewModule implements OnInit {
  categories: Category[];
  selectedCategory: Category;
  defaultDate: dateArray;
  defaultAddress: addressArray;
  model = new Order("", "", "", "", [this.defaultDate], [this.defaultAddress], "", "", "","","");
  actions = ["None", "Register", "Call", "Book Tickets"];
  // formResult: string[];
  user = new User('', '', '', '', '');
  myDate = new Date;
  message: String;
  category: String;
  status: '';
  disable: boolean;
  enableEdit = false;
  errorMessage: String;
  today = new Date().toJSON().split('T')[0];
  public latitude: number[] = [];
  public longitude: number[] = [];
  public zoom: number;
  public style = [];
  public moment: Date = new Date();
  public location: string[] = [];
  example = { first: '#checking', last: '' };
  validDate = new Date();
  numberOfDaysToAdd = 7;
  showrsvp: Boolean[] = [];
  rsvpdone: Boolean[] = [];
  count = 0;
  dateArrayCount = 0;
 myDates: Date[] = [];
 orderView: FormGroup;
f0 = 20;
s0 = 15;
f1 = 60;
s1 = 70;
f2 = 0;
s2 = 0;

showrsvpButton(i){
  this.showrsvp[i] = true;
  this.f0 = 10;
  this.f1 = 40;
  this.f2 = 40;
  this.s0 = 0;
  this.s1 = 50;
  this.s2 = 50;
}



rsvpCompleted(j){
  console.log("check");
  this.showrsvp[j] = false;
  this.rsvpdone[j] = true;
}


  @Input()
  responses: Array<any>;

  public hasBaseDropZoneOver: boolean = false;
  public uploader: FileUploader;
  private title: string;

  @Output()
  orderFinished = new EventEmitter();


  @ViewChildren("search")
  public searchElementRef: QueryList<ElementRef>;

  onSubmit({ value, valid }: { value: Order, valid: boolean }) {
    this.model = value
    this.model.type =  this.orderView.value.typeId.name;
     this.model.category =  this.orderView.value.category.name;
     this.model.typeUrl = this.orderView.value.category.url;
    this.doUpdate();
  }

  public address: Object;
  getAddress(place: Object) {
    this.address = place['formatted_address'];
    var location = place['geometry']['location'];
    var lat = location.lat();
    var lng = location.lng();
  }

  constructor(
    private categoryService: CategoryService,
    public dialog: MatDialog,
    private placeorderservice: PlaceOrderService,
    private http: Http,
    private fb: FormBuilder,
    public gs: generalService,
    public userService: UserService,
    private alertService: AlertService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private cloudinary: Cloudinary,
    private zone: NgZone,
    private router: Router,
  ) {
    this.responses = [];
    this.title = '';
  }

  getCategories(): void {
    this.categoryService
      .getCategories()
      .then(categories => this.categories = categories);
  }



  doUpdate() {
    this.userService
      .updateOrder(this.model, this.gs.currentorder._id)
      .subscribe(
        data => {
          this.router.navigate(["/account/orders"]);
          this.alertService.success("Updated order successfully", true);
        },
        error => {
          this.alertService.error(error);
        }
      );
  }


  // goForward(stepper: NgxStepperComponent) {
  //   stepper.next();
  // }
  setOrder(): void {

          // (<FormArray>this.order2.controls['addresses']).at(z).patchValue({ addressLatitude: this.latitude[z] });
          // (<FormArray>this.order2.controls['addresses']).at(z).patchValue({ addressLongitude: this.longitude[z] });
          // (<FormArray>this.order2.controls['addresses']).at(z).patchValue({ address: this.location[z] });
          setTimeout(() => {
          this.orderView.patchValue({ eventname: this.gs.currentorder.eventname });
          this.orderView.patchValue({ category: this.gs.currentorder.category });
          this.orderView.patchValue({ type: this.gs.currentorder.type });
          this.orderView.patchValue({ typeId: this.gs.currentorder.category });
          this.orderView.patchValue({ host: this.gs.currentorder.host});
          this.orderView.patchValue({ addresses: this.gs.currentorder.addresses});
          this.orderView.patchValue({ dates: this.gs.currentorder.dates});
          // this.orderView.patchValue({ addresses: this.gs.currentorder.addresses});
          // this.orderView.patchValue({ addresses: this.gs.currentorder.addresses});

        });

        }

  resetCategory():void{
    this.orderView.patchValue({ category: '' });
  }

  setEnableEdit(){
    setTimeout(() => {
        this.disable = false;
    })
  }
  createOrder({ value, valid }: { value: Order, valid: boolean }) {
    this.gs.order = value;
    this.userService.addOrder(value)
      .subscribe(
      order => {
        this.alertService.success('Order filled successful', true);
        this.orderFinished.emit()
      },
      error => {
        this.alertService.error(error);
      })
  }

  omit_special_char(event) {
    var k;
    k = event.charCode;  // k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }



  get CurrentOrder() { return JSON.stringify(this.model); }


  dateValidation(): any{
    //console.log("date: ",(<FormArray>this.order.controls['dates']).at(0))
    this.myDates[this.dateArrayCount] = (<FormArray>this.orderView.controls['dates']).at(this.dateArrayCount).get('date').value;
    this.dateArrayCount++;
    return this.myDates[this.dateArrayCount];
  }

  initDate() {
    this.validDate.setDate(this.validDate.getDate() + this.numberOfDaysToAdd);
    this.showrsvp.push(false);
    this.rsvpdone.push(false);
    return this.fb.group({
      date: ['', [Validators.required, CustomValidators.minDate(this.validDate)]],
      rsvpdate: ['', [CustomValidators.minDate(this.validDate)]],
      label: ['']
    });
  }

  // onChanges(): void {
  //   (<FormArray>this.order.controls['dates']).at(0).get('date').valueChanges.subscribe(val => {
  //     this.myDates[0] = val;
  //     console.log("val: ",val);
  //   });
  // }

  addNewDate() {
    const control = <FormArray>this.orderView.controls['dates'];
    control.push(this.initDate());
  }

  removeDate(i: number) {
    const control = <FormArray>this.orderView.controls['dates'];
    control.removeAt(i);
  }

  initAddress() {

    return this.fb.group({
      address: [''],
      addressLatitude: [''],
      addressLongitude: [''],
      label: [''],
      map: ['']
    });
  }

  addNewAddress() {
    const control = <FormArray>this.orderView.controls['addresses'];
    control.push(this.initAddress());
  }

  removeAddress(i: number) {
    const control = <FormArray>this.orderView.controls['addresses'];
    control.removeAt(i);
  }


  ngAfterViewInit():void{

  }



  ngOnInit(): void {
    let currentUrl = this.router.url;
    this.getCategories();
    this.setOrder();
    this.orderView = this.fb.group({
      eventname: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(3)]],
      category: ['', [Validators.required] ],
      host: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(3)]],
      callToAction: ['', [Validators.required]],
      dates: this.fb.array([
        this.initDate(),
      ]),
      addresses: this.fb.array([
        this.initAddress(),
      ]),
      banner: ['', []],
      userId: ['', []],
      type: ['', []],
      callToActionDetail: ['', []],
      typeId: ['', []],
      typeUrl: ['', []]
    });

    setTimeout(() => {
      this.disable = true;
      this.gs.showLara = true;
    });
    this.zoom = 15;
    this.latitude.push(6.459482);
    this.longitude[0] = 3.417974699999999;
    this.style = this.gs.style





    // Cloundinary API
    // Create the file uploader, wire it to upload to your account
    const uploaderOptions: FileUploaderOptions = {
      url: `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/upload`,
      // Upload files automatically upon addition to upload queue
      autoUpload: true,
      // Use xhrTransport in favor of iframeTransport
      isHTML5: true,
      // Calculate progress independently for each uploaded file
      removeAfterUpload: true,
      // XHR request headers
      headers: [
        {
          name: 'X-Requested-With',
          value: 'XMLHttpRequest'
        }
      ]
    };
    this.uploader = new FileUploader(uploaderOptions);

    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      // Add Cloudinary's unsigned upload preset to the upload form
      form.append('upload_preset', this.cloudinary.config().upload_preset);
      // Add built-in and custom tags for displaying the uploaded photo in the list
      let tags = 'myphotoalbum';
      if (this.title) {
        form.append('context', `photo=${this.title}`);
        tags = `myphotoalbum,${this.title}`;
      }
      form.append('tags', tags);
      form.append('file', fileItem);

      // Use default "withCredentials" value for CORS requests
      fileItem.withCredentials = false;
      return { fileItem, form };
    };

    // Insert or update an entry in the responses array
    const upsertResponse = fileItem => {

      // Run the update in a custom zone since for some reason change detection isn't performed
      // as part of the XHR request to upload the files.
      // Running in a custom zone forces change detection
      this.zone.run(() => {
        // Update an existing entry if it's upload hasn't completed yet

        // Find the id of an existing item
        const existingId = this.responses.reduce((prev, current, index) => {
          if (current.file.name === fileItem.file.name && !current.status) {
            return index;
          }
          return prev;
        }, -1);
        if (existingId > -1) {
          // Update existing item with new data
          this.responses[existingId] = Object.assign(this.responses[existingId], fileItem);
        } else {
          // Create new response
          this.responses.push(fileItem);
        }
      });
    };

    // Update model on completion of uploading a file
    this.uploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) =>
     { upsertResponse(
        {
          file: item.file,
          status,
          data: JSON.parse(response)
        }
      );
      this.orderView.patchValue({ banner: JSON.parse(response).secure_url});
    }

    // Update model on upload progress event
    this.uploader.onProgressItem = (fileItem: any, progress: any) =>
      upsertResponse(
        {
          file: fileItem.file,
          progress,
          data: {}
        }
      );
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude[0] = position.coords.latitude;
        this.longitude[0] = position.coords.longitude;
        this.zoom = 15;
        this.style = this.gs.style
      });
    }
  }


  onSelect(category: Category): void {
    this.selectedCategory = category;
  }

  updateTitle(value: string) {
    this.title = value;
  }

  // Delete an uploaded image
  // Requires setting "Return delete token" to "Yes" in your upload preset configuration
  // See also https://support.cloudinary.com/hc/en-us/articles/202521132-How-to-delete-an-image-from-the-client-side-
  deleteImage = function (data: any, index: number) {
    const url = `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/delete_by_token`;
    let headers = new Headers({ 'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest' });
    let options = new RequestOptions({ headers: headers });
    const body = {
      token: data.delete_token
    };
    this.http.post(url, body, options)
      .toPromise()
      .then((response) => {
        console.log(`Deleted image - ${data.public_id} ${response.json().result}`);
        // Remove deleted item for responses
        this.responses.splice(index, 1);
      }).catch((err: any) => {
        console.log(`Failed to delete image ${data.public_id} ${err}`);
      });
  };

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  getFileProperties(fileProperties: any) {
    // Transforms Javascript Object to an iterable to be used by *ngFor
    if (!fileProperties) {
      return null;
    }
    return Object.keys(fileProperties)
      .map((key) => ({ 'key': key, 'value': fileProperties[key] }));
  }
}
