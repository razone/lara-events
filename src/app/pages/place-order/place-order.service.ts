import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions}  from '@angular/http';
import {Order, User, Login} from "../place-order/order"
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PlaceOrderService {
    getUser(): void{};

    constructor(private http: Http){}

private registerUrl = '/api/customer/register';
private orderUrl = '/api/customer/order';
private loginUrl = '/api/customer/authenticate';

     addUser(body: Object): Observable<User> {
    let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.registerUrl, body, options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }  

       addOrder(body: Object): Observable<Order> {
    let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.orderUrl, body, options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    } 
    login(body: Object): Observable<Login> {
        let bodyString = JSON.stringify(body); // Stringify payload
            let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            let options       = new RequestOptions({ headers: headers }); // Create a request option
    
            return this.http.post(this.loginUrl, body, options) // ...using post request
                             .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                             .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
        } 
}