import { Category } from './category';

export const Categories: Category[] = [
    {
        id: 1,
        name: 'Private Events',
        type: [{ name: 'Weddings', url: 'http://res.cloudinary.com/razone/image/upload/v1511244213/menu_img/Weddings_qjt5ed.svg' },
        { name: 'Birthday Party', url: 'http://res.cloudinary.com/razone/image/upload/v1511244206/menu_img/Birthday_Party_mvc4zo.svg' },
        { name: 'Festive Gathering', url: 'http://res.cloudinary.com/razone/image/upload/v1511244207/menu_img/Festive_Gathering_j3pd7e.svg' },
        { name: 'Exhibition & Tradeshow', url: 'http://res.cloudinary.com/razone/image/upload/v1511244206/menu_img/Exhibition_Tradeshow_bdlim1.svg' }]
    },
    {
        id: 2,
        name: 'Corporate Events',
        type: [
            { name: 'Conference', url: 'http://res.cloudinary.com/razone/image/upload/v1511244206/menu_img/Conference_omdvnp.svg' },
            { name: 'Networking', url: 'http://res.cloudinary.com/razone/image/upload/v1511244211/menu_img/Networking_b0bzvn.svg' },
            { name: 'Product launch', url: 'http://res.cloudinary.com/razone/image/upload/v1511244211/menu_img/Product_launch_fiulew.svg' }]
    },
    {
        id: 3,
        name: 'Charity Events',
        type: [
            { name: 'Charity Events', url: 'http://res.cloudinary.com/razone/image/upload/v1511243737/menu_img/Charity_event_rcdoai.svg' }]
    },
    {
        id: 4,
        name: 'Live Events',
        type: [
            { name: 'Music show', url: 'http://res.cloudinary.com/razone/image/upload/v1511244210/menu_img/Music_show_zukhdt.svg' },
            { name: 'Comedy show', url: 'http://res.cloudinary.com/razone/image/upload/v1511244206/menu_img/Comedy_show_mpn1ap.svg' },
            { name: 'Sporting Event', url: 'http://res.cloudinary.com/razone/image/upload/v1511244212/menu_img/Sporting_Event_boh3of.svg' },
            { name: 'Festival', url: 'http://res.cloudinary.com/razone/image/upload/v1511244206/menu_img/Festival_qktfdh.svg' }]
    }
];