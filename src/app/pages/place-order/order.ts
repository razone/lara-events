export class Photo {
  public_id: string;
  context: any;
}



export class Order {
 constructor(
   public eventname: string,
   public category: string,
   public host: string,
   public callToAction: string,
   public dates: dateArray[],
   public addresses: addressArray[],
   public userId: string,
   public type: string,
   public typeUrl: string,
   public banner?: string,
   public id?: string,
   public callToActionDetail?: string,
   public status?: string,
   public videoUrl?: string,
   public deletedOrder?: boolean,
   public ctaUrl?: string,
   public _id?: string
 ) { }
 }

 export class User {
 constructor(
   public firstname: string,
   public lastname: string,
   public email: string,
   public password: string,
   public telephone: string
 ) { }
 }

 export class fees{
   constructor(
     public paid: boolean,
     public status: string,
     public color: string
   ){}
 }
 export interface Order {
    eventname: string,
    category: string,
    host: string,
    callToAction: string,
    dates: dateArray[],
    addresses: addressArray[],
    banner?: string,
    userId: string,
    type: string,
    typeUrl: string,
    callToActionDetail?: string,
    status?: string
    videoUrl?: string,
    deletedOrder?: boolean,
    ctaUrl?: string,
    _id?: string
 }

 export interface dateArray{
   date: string,
   rsvpdate: string,
   label?: string
 }

 export interface addressArray{
  address: string,
  addressLatitude?: string,
  addressLongitude?: string,
  label?: string,
  map?: string
 }

 export class dateArray{
   constructor(
  date: string,
  rsvpdate: string,
  label?: string
   ){}
}

export class addressArray{
  constructor(
    address: string,
    addressLatitude: string,
    addressLongitude: string,
    label?: string,
    map?: string
  ){}
}

 export interface User{
   firstname: string;
   lastname: string;
   email: string;
   password: string;
   telephone: string;
 }

 export interface Login{
   email: string;
   password: string;
 }

 export class Orders {
  constructor(
    public number: string,
    public name: string,
    public id: string
  ) { }
  }

  export class RSVP {
    constructor(
      public number: string,
      public name: string
    ) { }
    }

