import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Categories } from './mock-categories';
import { Category } from './category'

@Injectable()
export class CategoryService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private categoryUrl = 'api/categories';  // URL to web api

  constructor(private http: Http) { }

  getCategories(): Promise<Category[]> {
    // return this.http.get(this.categoryUrl)
    //            .toPromise()
    //            .then(response => response.json().data as Category[])
    //            .catch(this.handleError);

    return Promise.resolve(Categories);
  }


 // getCategory(id: number): Promise<Category> {
    // const url = `${this.categoryUrl}/${id}`;
    // return this.http.get(url)
    //   .toPromise()
    //   .then(response => response.json().data as Category)
    //   .catch(this.handleError);
 // }

//   delete(id: number): Promise<void> {
//     const url = `${this.categoryUrl}/${id}`;
//     return this.http.delete(url, {headers: this.headers})
//       .toPromise()
//       .then(() => null)
//       .catch(this.handleError);
//   }

//   create(name: string): Promise<Category> {
//     return this.http
//       .post(this.categoryUrl, JSON.stringify({name: name}), {headers: this.headers})
//       .toPromise()
//       .then(res => res.json().data as Category)
//       .catch(this.handleError);
//   }

//   update(hero: Category): Promise<Category> {
//     const url = `${this.categoryUrl}/${hero.id}`;
//     return this.http
//       .put(url, JSON.stringify(hero), {headers: this.headers})
//       .toPromise()
//       .then(() => hero)
//       .catch(this.handleError);
//   }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

