import {
  Component,
  QueryList,
  ViewChildren,
  ElementRef,
  NgZone,
  Input,
  NgModule,
  OnInit,
  ViewChild,
  Inject,
  forwardRef,
  Output,
  EventEmitter
} from "@angular/core";
import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { SvgViewerModule } from "../../shared/svg-viewer/svg-viewer";
import { MatButtonModule } from "@angular/material";
import { RouterModule, Router } from "@angular/router";
import { ComponentPageTitle } from "../page-title/page-title";
import {
  MatSelectModule,
  MatIconModule,
  MatRadioModule,
  MatDatepickerModule,
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatInputModule,
  MatIconRegistry
} from "@angular/material";
import {
  FormsModule,
  FormArray,
  Validators,
  FormControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule
} from "@angular/forms";
import { Order, User, dateArray, addressArray } from "../place-order/order";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
//import {ImageUploadModule} from 'angular2-image-upload';
import { AlertService } from "../../shared/_services/alert.service";
import { UserService } from "../../shared/_services/user.service";
import { GooglePlaceModule } from "angular2-google-place";
import { SignUpDialogModule } from "../sign-up/sign-up";
import { PlaceOrderService } from "../place-order/place-order.service";
import { generalService } from "../homepage/general.service";
import { Observable } from "rxjs/Observable";
import {} from "googlemaps";
import { MapsAPILoader } from "@agm/core";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {
  FileUploader,
  FileUploadModule,
  FileUploaderOptions,
  ParsedResponseHeaders
} from "ng2-file-upload";
import "rxjs/add/operator/toPromise";
import { Cloudinary } from "@cloudinary/angular-4.x";
import { CustomValidators } from "ng2-validation";

import { Category } from "../place-order/category";
import { CategoryService } from "../place-order/category.service";

@Component({
  selector: "admin-view-order",
  //directives: [GoogleplaceDirective],
  templateUrl: "admin-view-order.html",
  styleUrls: ["admin-view-order.scss"]
})
export class AdminViewOrderModule implements OnInit {
  categories: Category[];
  selectedCategory: Category;
  defaultDate: dateArray;
  defaultAddress: addressArray;
  addresses: addressArray[] = [new addressArray("", "", "")];
  model = new Order(
    "",
    "",
    "",
    "",
    [this.defaultDate],
    [this.defaultAddress],
    "",
    "",
    "",
    "",
    ""
  );
  actions = ["None", "Register", "Call", "Book Tickets"];
  // formResult: string[];
  user = new User("", "", "", "", "");
  myDate = new Date();
  message: String;
  category: String;
  status: "";
  errorMessage: String;
  today = new Date().toJSON().split("T")[0];
  public latitude: number[] = [];
  public longitude: number[] = [];
  public zoom: number;
  public style = [];
  public moment: Date = new Date();
  public location: string[] = [];
  count = 0;
  dateArrayCount = 0;
  deliveryDate: Date = new Date();
  numberOfDaysToAdd = 10;
  categoryName: String;

  @Input() responses: Array<any>;

  public hasBaseDropZoneOver: boolean = false;
  public uploader: FileUploader;
  private title: string;

  @Output() orderFinished = new EventEmitter();

  @ViewChildren("search") public searchElementRef: QueryList<ElementRef>;

  order2: FormGroup;

  public address: Object;
  getAddress(place: Object) {
    this.address = place["formatted_address"];
    var location = place["geometry"]["location"];
    var lat = location.lat();
    var lng = location.lng();
  }

  constructor(
    private categoryService: CategoryService,
    public dialog: MatDialog,
    private placeorderservice: PlaceOrderService,
    private http: Http,
    private fb: FormBuilder,
    public gs: generalService,
    public userService: UserService,
    private alertService: AlertService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private cloudinary: Cloudinary,
    private zone: NgZone,
    private router: Router
  ) {
    this.responses = [];
    this.title = "";
  }

  getCategories(): void {
    this.categoryService
      .getCategories()
      .then(categories => (this.categories = categories));
  }

  // goForward(stepper: NgxStepperComponent) {
  //   stepper.next();
  // }

  newOrder(): void {
    // (<FormArray>this.order2.controls['addresses']).at(z).patchValue({ addressLatitude: this.latitude[z] });
    // (<FormArray>this.order2.controls['addresses']).at(z).patchValue({ addressLongitude: this.longitude[z] });
    // (<FormArray>this.order2.controls['addresses']).at(z).patchValue({ address: this.location[z] });

    console.log("gs: ", this.gs.currentorder);
    setTimeout(() => {
      this.order2.patchValue({ eventname: this.gs.currentorder.eventname });
      this.order2.patchValue({ category: this.gs.currentorder.typeUrl });
      this.order2.patchValue({ addresses: this.gs.currentorder.addresses });
      this.addresses = this.gs.currentorder.addresses;
      this.categoryName = this.gs.currentorder.category;
      let cDate: Date = this.gs.currentorder.createdDate;
      this.deliveryDate.setDate(
        this.deliveryDate.getDate() + this.numberOfDaysToAdd
      );
      if (this.gs.currentorder.mobilizationFee) {
        this.order2.patchValue({
          mobilizationFee: "Deposited made: NGN25, 000"
        });
        if (this.gs.currentorder.deliveryFee) {
          this.order2.patchValue({ deliveryFee: "Deposited made: NGN75, 000" });
        } else {
          this.order2.patchValue({
            deliveryFee: "Awaiting payment: NGN75, 000"
          });
        }
      } else {
        this.order2.patchValue({ deliveryFee: "Awaiting payment: NGN75, 000" });
        this.order2.patchValue({
          mobilizationFee: "Awaiting payment: NGN25, 000"
        });
      }
    });
    this.userService.getUser(this.gs.currentorder.userId).subscribe(
      data => {
        this.order2.patchValue({ name: data.name });
        this.order2.patchValue({ email: data.email });
        this.order2.patchValue({ mobile: data.mobile });
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(DeleteDialog, {
      width: "250px"
      // data: { name: this.name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("done!");
        this.deleteOrder();
      }
    });
  }

  resetCategory(): void {
    this.order2.patchValue({ category: "" });
  }

  updateStatus(newStatus: string): void {
    this.model.status = newStatus;
    this.doUpdate();
  }

  updateVideoUrl(url: string): void {
    this.model.videoUrl = url;
    this.doUpdate();
  }

  doUpdate() {
    this.userService
      .updateOrder(this.model, this.gs.currentorder._id)
      .subscribe(
        data => {
          this.router.navigate(["/admin/list"]);
          this.alertService.success("Login successful", true);
        },
        error => {
          this.alertService.error(error);
        }
      );
  }


  deleteOrder(): void {
    this.model.deletedOrder = true;
    this.doUpdate();
  }

  omit_special_char(event) {
    var k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  ngAfterViewInit(): void {
    this.newOrder();

    this.searchElementRef.changes.subscribe(() => {
      // will be called every time an item is added/removed
      this.mapsAPILoader.load().then(() => {
        this.searchElementRef.forEach(search => {
          let autocomplete = new google.maps.places.Autocomplete(
            search.nativeElement,
            {
              types: []
            }
          );
          autocomplete.setComponentRestrictions({ country: ["ng"] });

          if ("geolocation" in navigator) {
            console.log("in here");
            navigator.geolocation.getCurrentPosition(position => {
              this.latitude[this.count + 1] = position.coords.latitude;
              this.longitude[this.count + 1] = position.coords.longitude;
              this.zoom = 15;
              this.style = this.gs.style;
            });
          }
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              //get the place result
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();

              //verify result
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }

              //console.log("place: ",place);
              //set latitude, longitude and zoom
              this.location[this.count] = place.formatted_address;
              this.latitude[this.count] = place.geometry.location.lat();
              this.longitude[this.count] = place.geometry.location.lng();
              this.zoom = 15;
              this.style = this.gs.style;
              this.count++;
              console.log("count:", this.count);
            });
          });
        });
      });
    });
  }

  initDate() {
    return this.fb.group({
      date: ["", [Validators.required]],
      rsvpdate: [""],
      label: [""]
    });
  }

  addNewDate() {
    const control = <FormArray>this.order2.controls["dates"];
    control.push(this.initDate());
  }

  removeDate(i: number) {
    const control = <FormArray>this.order2.controls["dates"];
    control.removeAt(i);
  }

  initAddress() {
    return this.fb.group({
      address: [""],
      addressLatitude: [""],
      addressLongitude: [""],
      label: [""],
      map: [""]
    });
  }

  addNewAddress() {
    const control = <FormArray>this.order2.controls["addresses"];
    control.push(this.initAddress());
  }

  removeAddress(i: number) {
    const control = <FormArray>this.order2.controls["addresses"];
    control.removeAt(i);
  }

  ngOnInit(): void {
    this.getCategories();

    this.order2 = this.fb.group({
      eventname: [{ value: "", disabled: true }, []],
      category: ["", []],
      name: [{ value: "", disabled: true }, []],
      email: [{ value: "", disabled: true }, []],
      mobile: [{ value: "", disabled: true }, []],
      mobilizationFee: ["", []],
      deliveryFee: ["", []],
      addresses: this.fb.array([this.initAddress()]),
      date: ["", []]
    });

    this.zoom = 15;
    this.latitude.push(6.459482);
    this.longitude[0] = 3.417974699999999;
    this.style = this.gs.style;

    // Cloundinary API
    // Create the file uploader, wire it to upload to your account
    const uploaderOptions: FileUploaderOptions = {
      url: `https://api.cloudinary.com/v1_1/${
        this.cloudinary.config().cloud_name
      }/upload`,
      // Upload files automatically upon addition to upload queue
      autoUpload: true,
      // Use xhrTransport in favor of iframeTransport
      isHTML5: true,
      // Calculate progress independently for each uploaded file
      removeAfterUpload: true,
      // XHR request headers
      headers: [
        {
          name: "X-Requested-With",
          value: "XMLHttpRequest"
        }
      ]
    };
    this.uploader = new FileUploader(uploaderOptions);

    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      // Add Cloudinary's unsigned upload preset to the upload form
      form.append("upload_preset", this.cloudinary.config().upload_preset);
      // Add built-in and custom tags for displaying the uploaded photo in the list
      let tags = "videodirection";
      if (this.title) {
        form.append("context", `photo=${this.title}`);
        tags = `myphotoalbum,${this.title}`;
      }
      form.append("tags", tags);
      form.append("file", fileItem);

      // Use default "withCredentials" value for CORS requests
      fileItem.withCredentials = false;
      return { fileItem, form };
    };

    // Insert or update an entry in the responses array
    const upsertResponse = fileItem => {
      // Run the update in a custom zone since for some reason change detection isn't performed
      // as part of the XHR request to upload the files.
      // Running in a custom zone forces change detection
      this.zone.run(() => {
        // Update an existing entry if it's upload hasn't completed yet

        // Find the id of an existing item
        const existingId = this.responses.reduce((prev, current, index) => {
          if (current.file.name === fileItem.file.name && !current.status) {
            return index;
          }
          return prev;
        }, -1);
        if (existingId > -1) {
          // Update existing item with new data
          this.responses[existingId] = Object.assign(
            this.responses[existingId],
            fileItem
          );
        } else {
          // Create new response
          this.responses.push(fileItem);
        }
      });
    };

    // Update model on completion of uploading a file
    this.uploader.onCompleteItem = (
      item: any,
      response: string,
      status: number,
      headers: ParsedResponseHeaders
    ) => {
      upsertResponse({
        file: item.file,
        status,
        data: JSON.parse(response)
      });
      this.updateVideoUrl(JSON.parse(response).secure_url);
    };

    // Update model on upload progress event
    this.uploader.onProgressItem = (fileItem: any, progress: any) =>
      upsertResponse({
        file: fileItem.file,
        progress,
        data: {}
      });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude[0] = position.coords.latitude;
        this.longitude[0] = position.coords.longitude;
        this.zoom = 15;
        this.style = this.gs.style;
      });
    }
  }

  onSelect(category: Category): void {
    this.selectedCategory = category;
  }

  updateTitle(value: string) {
    this.title = value;
  }

  // Delete an uploaded image
  // Requires setting "Return delete token" to "Yes" in your upload preset configuration
  // See also https://support.cloudinary.com/hc/en-us/articles/202521132-How-to-delete-an-image-from-the-client-side-
  deleteImage = function(data: any, index: number) {
    const url = `https://api.cloudinary.com/v1_1/${
      this.cloudinary.config().cloud_name
    }/delete_by_token`;
    let headers = new Headers({
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest"
    });
    let options = new RequestOptions({ headers: headers });
    const body = {
      token: data.delete_token
    };
    this.http
      .post(url, body, options)
      .toPromise()
      .then(response => {
        console.log(
          `Deleted image - ${data.public_id} ${response.json().result}`
        );
        // Remove deleted item for responses
        this.responses.splice(index, 1);
      })
      .catch((err: any) => {
        console.log(`Failed to delete image ${data.public_id} ${err}`);
      });
  };

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  getFileProperties(fileProperties: any) {
    // Transforms Javascript Object to an iterable to be used by *ngFor
    if (!fileProperties) {
      return null;
    }
    return Object.keys(fileProperties).map(key => ({
      key: key,
      value: fileProperties[key]
    }));
  }
}

@Component({
  selector: "delete-dialog",
  templateUrl: "delete-dialog.html"
})
export class DeleteDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
