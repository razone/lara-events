import { Component, NgModule, OnInit, ViewChild } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SvgViewerModule } from '../../shared/svg-viewer/svg-viewer';
import { MatButtonModule, MatSortModule, MatSort } from '@angular/material';
import { MatSelectModule, MatIconModule, MatTableModule, MatCheckboxModule, MatIconRegistry, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RouterModule, Router } from '@angular/router';
import {DataSource} from '@angular/cdk/collections';
import { Order, fees, dateArray, addressArray } from '../place-order/order'
import { AlertService } from '../../shared/_services/alert.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { UserService } from '../../shared/_services/user.service';
import { generalService } from '../homepage/general.service';
import { Observable } from 'rxjs/Observable';
import { Angular4PaystackModule } from 'angular4-paystack';
import {MatPaginator} from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';

/**
 * @title Basic table
 */
@Component({
  selector: 'admin-view',
  styleUrls: ['admin-view.scss'],
  templateUrl: 'admin-view.html',
})
export class AdminViewModule implements OnInit{
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private http: Http,
    public gs: generalService,
    private userService: UserService,
    private alertService: AlertService,
    public router: Router){}


  displayedColumns = ['eventname', 'address', 'date', 'action'];
  dataSource: AdminViewDataSource | null;
  adminDatabase: AdminViewHttpDao | null;

  defaultDate: dateArray;
  defaultAddress: addressArray;
  model = new Order("", "", "", "", [ this.defaultDate], [this.defaultAddress], "", "","");
  latestOrder: any;
  todayDate: Date = new Date();
  count = 0;
  reviewCount = 0;
  totalCount = 0;
progressCount = 0;


getInfo(): void{

  this.userService.getOrdersInfo()
  .subscribe(
    data => {
      //var counts = JSON.stringify(data);
      this.totalCount = data.totalCount;
      this.reviewCount = data.reviewCount;
      this.progressCount = data.processingCount;
    },
    error => {
      this.alertService.error(error);
    })
}
  //getOrder(): void {
    // if(this.gs.loggedIn){
    //   this.userService.getOrders()
    //     .subscribe(

    //     orders => {
    //       //this.gs.saveOrders(JSON.stringify(orders));
    //       this.gs.saveOrders(JSON.stringify(orders))
    //       console.log("Orders array: ",orders);
    //       console.log("Service Orders array: ",this.gs.orders)
    //     },
    //     error => {
    //       this.alertService.error(error);
    //     })
    //   }
    // }

    openOrder(element: any):void{
      this.gs.currentorder = element;
      this.router.navigate(['/admin/view']);
    }


    ngOnInit(): void {
     // this.getOrder();
     this.getInfo();
      this.adminDatabase = new AdminViewHttpDao(this.http);
      this.dataSource = new AdminViewDataSource(
        this.adminDatabase!, this.paginator, this.sort);
    }
}

export interface OrderApi {
  orders: Order[];
  total_count: number;
}

//data: String[];


/**
 * Data source to provide what data should be rendered in the table. The observable provided
 * in connect should emit exactly the data that should be rendered by the table. If the data is
 * altered, the observable should emit that new set of data on the stream. In our case here,
 * we return a stream that contains only one set of data that doesn't change.
 */
export class AdminViewHttpDao {

  constructor(private http: Http) {}

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('laraCurrentUser'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    }
}

    getOrders(sort: string, order: string, page: number, limit: number): Observable<OrderApi> {

      const href = '/api/orders/all/pages';
      const requestUrl =
        `${href}?sort=${sort}&order=${order}&page=${page + 1}&limit=${limit}`;

      return this.http.get(requestUrl, this.jwt()).map(response => response.json() as OrderApi);
   }

}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleHttpDao. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class AdminViewDataSource extends DataSource<Order> {
  // The number of orders returned by the service.
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  constructor(private adminDatabase: AdminViewHttpDao,
              private paginator: MatPaginator,
              private sort: MatSort) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Order[]> {

    const displayDataChanges = [
      this.paginator.page,
      this.sort.sortChange
    ];

    let limit: number = 5

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    return Observable.merge(...displayDataChanges)
      .startWith(null)
      .switchMap(() => {
        setTimeout(() => {
          this.isLoadingResults = true;
  });
        return this.adminDatabase.getOrders(
          this.sort.active, this.sort.direction, this.paginator.pageIndex, 5);
      })
      .map(data => {
        // Flip flag to show that loading has finished.
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.resultsLength = data.total_count;
        return data.orders;
      })
      .catch(() => {
        this.isLoadingResults = false;
        // Catch if the Order API has reached its rate limit. Return empty data.
        this.isRateLimitReached = true;
        return Observable.of([]);
      });
  }

  disconnect() {}
}
