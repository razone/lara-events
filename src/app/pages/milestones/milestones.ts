import {Component, OnInit} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {Order, fees, dateArray, addressArray} from '../place-order/order';
import {AlertService} from '../../shared/_services/alert.service';
import {Http} from '@angular/http';
import {UserService} from '../../shared/_services/user.service';
import {generalService} from '../homepage/general.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

/**
 * @title Basic table
 */
@Component({
    selector: 'milestones',
    styleUrls: ['milestones.scss'],
    templateUrl: 'milestones.html',
})
export class MilestonesModule implements OnInit {

    constructor(private http: Http,
                public gs: generalService,
                private userService: UserService,
                private alertService: AlertService) {
    }


    displayedColumns = ['description', 'amount', 'status', 'action'];
    dataSource = new MilestoneDataSource();
    card: any;
    bank: any;
    feeStatus = ['Pending', 'In Progress', 'Done'];
    mobilization: fees = new fees(false, this.feeStatus[1], 'grey');
    delivery: fees = new fees(false, this.feeStatus[0], 'grey');
    defaultDate: dateArray;
    defaultAddress: addressArray;
    model = new Order('', '', '', '', [this.defaultDate], [this.defaultAddress], '', '', '');
    latestOrder: any;
    date: Date = new Date();
    rsvpDate: Date = new Date();
    ref: any;
    deliveryStatus: string;
    mobilizationStatus: string;

    //Used for mobile view
    dataForMobile = data;


    amountDue(): number {
        // var appKey = JSON.parse(this.gs.appconfig)
        if (!this.mobilization.paid) {
            return 1500000;
        }
        else if (!this.delivery.paid) {
            return 3500000;
        }
        return 0;
    }

    generateReference(): string {
        var value = '12345' + this.getRandomIntInclusive(10, 5900332234);
        this.ref = value;
        return value;
    }

    getRandomIntInclusive(min, max): Number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
    }

    paymentDone(event: any): void {
        this.generateReference();
        if (!this.mobilization.paid) {
            this.mobilization.paid = true;
            this.mobilization.status = this.mobilization.status + 1;
            this.delivery.status = this.delivery.status + 1;
            this.mobilization.color = 'green';
        }
        else if (this.mobilization.paid) {
            this.delivery.paid = true;
            this.delivery.status = this.delivery.status + 2;
            this.mobilization.color = 'green';
        }

        console.log('event from paystack: ', event);
    }

    getOrder(): void {
        if (this.gs.loggedIn) {
            this.userService.getLatestOrder()
                .subscribe(
                    order => {
                        this.gs.currentorders = order;
                        this.gs.currentEventName = order.eventname;
                        this.gs.orderDate = order.dates[0].date;
                        this.gs.deliveryDate.setDate(this.gs.orderDate.getDate() + 7);
                        console.log("date: ",this.gs.orderDate)
                        // this.latestOrder = JSON.stringify(order);
                        // this.date = this.latestOrder.date;
                        // this.rsvpDate = this.latestOrder.rsvpDate;
                        // this.mobilization = this.latestOrder.mobilizationFee
                        // this.delivery = this.latestOrder.finalDeliveryFee
                    },
                    error => {
                        this.alertService.error(error);
                    });
        }
        console.log('latest: ', this.date);
        console.log('latest: ', this.rsvpDate);
    }


    ngOnInit(): void {
        this.getOrder();
        this.generateReference();
        console.log('mobilize: ', this.mobilization);
    }
}

export interface Element {
    description: string;
    amount: string;
    status: string;
    action: string;
    color: string;
    date: Date;
    disable: boolean
}


export const data: Element[] = [
    {description: 'Mobilization Fee', amount: 'N25, 000', status: '', action: 'H', color: '', date: this.date, disable: false},
    {
        description: 'Final Deliverable Fee',
        amount: 'N75, 000',
        status: '',
        action: 'He',
        color: '',
        date: this.rsvpDate,
        disable: true
    },
];

/**
 * Data source to provide what data should be rendered in the table. The observable provided
 * in connect should emit exactly the data that should be rendered by the table. If the data is
 * altered, the observable should emit that new set of data on the stream. In our case here,
 * we return a stream that contains only one set of data that doesn't change.
 */
export class MilestoneDataSource extends DataSource<any> {
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<Element[]> {
        return Observable.of(data);
    }

    disconnect() {
    }
}
