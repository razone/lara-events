import { Component, NgModule, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SvgViewerModule } from '../../shared/svg-viewer/svg-viewer';
import { MatButtonModule } from '@angular/material';
import { RouterModule, Router } from '@angular/router';
import { generalService } from '../homepage/general.service'
import { HttpModule } from '@angular/http';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { ComponentPageTitle } from '../page-title/page-title';
import { MatSelectModule, MatIconModule, MatDialogModule, MatIconRegistry, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormsModule, Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Login } from "../place-order/order";
//import { PlaceOrderService } from "../place-order/place-order.service"
import { AlertService } from '../../shared/_services/alert.service';
import { UserService } from '../../shared/_services/user.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  templateUrl: './login.html',
  styleUrls: ['login.scss'],
})


export class LoginDialogModule implements OnInit {

  message: String;
  status: '';
  errorMessage: String;
  //user = new User('','','','','');


  user: FormGroup;

  onSubmit({ value, valid }: { value: Login, valid: boolean }) {
    console.log(value, valid);
    this.userService.login(value)
      .subscribe(
      data => {
            localStorage.setItem('laraCurrentUser', JSON.stringify(data));
            this.loginDailog.close();
            this.gs.toogleLoggedIn();

        this.router.navigate(['/']);
            this.alertService.success('Login successful', true);
      
      },
      error => {
        this.alertService.error(error);
      })
    //      this.dialogRef.close();           			
  }

  constructor(
    public loginDailog: MatDialogRef<LoginDialogModule>,
    private http: Http,
    private fb: FormBuilder,
    public Matdialog: MatDialog,
    public dr: MatDialogRef<any>,
    private gs: generalService,
    private userService: UserService,
    private alertService: AlertService,
  public router: Router) { }


  onNoClick(): void {
    this.loginDailog.close();
  }



  ngOnInit() {
    this.user = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }


}