import { Component, NgModule, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SvgViewerModule } from '../../shared/svg-viewer/svg-viewer';
import { MatButtonModule } from '@angular/material';
import { RouterModule, Router } from '@angular/router';
import { generalService } from '../homepage/general.service'
import { HttpModule } from '@angular/http';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { ComponentPageTitle } from '../page-title/page-title';
import { MatSelectModule, MatIconModule, MatDialogModule, MatIconRegistry, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormsModule, Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Order, User } from "../place-order/order";
//import { PlaceOrderService } from "../place-order/place-order.service"
import { AlertService } from '../../shared/_services/alert.service';
import { UserService } from '../../shared/_services/user.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.html',
  styleUrls: ['sign-up.scss'],
})


export class SignUpDialogModule implements OnInit {

  email: '';
  lname: '';
  fname: '';
  telephone: '';
  message: String;
  status: '';
  errorMessage: String;
  //user = new User('','','','','');

  omit_special_char(event)
  {   
     var k;  
     k = event.charCode;  //         k = event.keyCode;  (Both can be used)
     return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
  }

  numberValidate(event: any) {
    const pattern = /\b^(07|08|09)\d{8}/
    let inputChar = String.fromCharCode(event.charCode);
    if ( !pattern.test(inputChar)) {
    event.preventDefault();
    }
}
  user: FormGroup;

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    console.log(value, valid);
    this.userService.addUser(value)
      .subscribe(
      user => {
        //var res = JSON.stringify(user);
        localStorage.setItem('laraCurrentUser', JSON.stringify(user));
        //this.alertService.success('Registration successful', true);
        this.gs.toogleLoggedIn();
        this.dialogRef.close();
        this.router.navigate(['/']);
        this.alertService.success('Login successful', true);

      },
      error => {
        this.alertService.error(error);
      })
  }

  constructor(
    public dialogRef: MatDialogRef<SignUpDialogModule>,
    @Inject(MAT_DIALOG_DATA) public data: { user: User, order: Order }, private http: Http,
    private fb: FormBuilder,
    public Matdialog: MatDialog,
    public dr: MatDialogRef<any>,
    private gs: generalService,
    private userService: UserService,
    private alertService: AlertService,
    public router: Router) { }

  // signUp(): void {
  //   this.userService.addUser(this.user)
  //     .subscribe(
  //     user => {
  //       this.message = user.email;
  //       this.alertService.success('Registration successful', true);
  //       //this.router.navigate(['/login']);
  //     },
  //     error => {
  //       //this.errorMessage = <any>error);
  //       this.alertService.error(error);
  //     })

  // }

  onNoClick(): void {
    this.dialogRef.close();
  }



  ngOnInit() {
    this.user = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(3)]],
      lastname: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      telephone: ['', [Validators.required, Validators.minLength(10), Validators.pattern(/\b^(07|08|09)\d{8}/)]]
    });
  }

  //     public selectCampaign(): void {
  //     this.steppers.showFeedback('Processing, please wait ...');
  //     setTimeout(() => {
  //       if (this.campaign) {
  //         this.steppers.clearError();
  //         this.steppers.next();
  //       } else {
  //         this.campaign = !this.campaign;
  //         this.steppers.error('Wrong campaign');
  //       }
  //     }, 1000);
  //   }



}