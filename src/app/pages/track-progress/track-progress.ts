import { Component, NgModule, OnInit } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MatButtonModule , MatProgressBarModule, MatListModule} from '@angular/material';
import { AlertService } from '../../shared/_services/alert.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { UserService } from '../../shared/_services/user.service';
import { generalService } from '../homepage/general.service';
import { QRCodeModule } from 'angular2-qrcode';

/**
 * @title Basic table
 */
@Component({
  selector: 'track-progress',
  styleUrls: ['track-progress.scss'],
  templateUrl: 'track-progress.html',
})
export class TrackProgressModule {
 constructor(
  public gs: generalService){}

  items: Items[] = [
    { description: "Custom Hashtags & unique directions link", number: "one", ready: true},
    { description: "Public transit directions and fare quotes to 'this.gs.currentEventName'", number:"two", ready: true},
    { description: "Acquire QR Code", number: "3", ready: true},
    { description: "RSVP for 'this.gs.currentEventName'", number: "4", ready: false},
    { description: "Compare and book rides on Ride-Share service to 'this.gs.currentEventName'", number: "5", ready: false},
    { description: "Video Guided Tour service (street level) to 'this.gs.currentEventName'", number:"6", ready: false}
      ]


}



 export class Items {
 constructor(
   public number: string,
   public description: string,
   public ready: boolean
 ) { }
 }

