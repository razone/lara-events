import { Component, OnInit } from '@angular/core';
import { Orders } from '../../place-order/order';
import { UserService } from '../../../shared/_services/user.service';
import { generalService } from '../../homepage/general.service';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'current-orders',
  templateUrl: './current-orders.html',
  styleUrls: ['./orders.scss'],
})
export class CurrentOrdersComponent implements OnInit {

  constructor(public router: Router,
    private gs: generalService) { }

  orders = this.gs.currentorders;
  currentOrders: Orders = new Orders('','','')
  items = [];

  viewSummary(itemId: String){
    var myOrder = this.gs.currentorders
    for (var i = 0; i < myOrder.length; i++) {
      if(myOrder[i]._id == itemId){
        this.gs.currentorder = myOrder[i]
        this.router.navigateByUrl('/order');
      }
    }
  }
  ngOnInit() {
    if(this.gs.currentorders){
    for (var i = 0; i < this.gs.currentorders.length; i++) {
      var obj = this.gs.currentorders[i];
       this.currentOrders.name = obj.eventname;
       this.currentOrders.number = this.gs.getNumberString(i + 1);
       this.currentOrders.id = obj._id;
       this.items[i] = this.currentOrders;
    }
  }
  }
}
