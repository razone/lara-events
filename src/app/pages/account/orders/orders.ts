import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {SvgViewerModule} from '../../../shared/svg-viewer/svg-viewer';
import {MatButtonModule, MatTabsModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterModule} from '../../../shared/footer/footer';
import {RouterModule} from '@angular/router';
import {ComponentPageTitle} from '../../page-title/page-title';
import {CurrentOrdersComponent} from '../orders/current-orders';
import {PastOrdersComponent} from '../orders/past-orders';
import { AlertService } from '../../../shared/_services/alert.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserService } from '../../../shared/_services/user.service';
import { generalService } from '../../homepage/general.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';


@Component({
  selector: 'account-orders',
  templateUrl: './orders.html',
  styleUrls: ['./orders.scss'],
})
export class OrdersComponent implements OnInit {
  constructor(
    private http: Http,
    private gs: generalService,
    private userService: UserService,
    private alertService: AlertService){}



  getAllOrder(): void {

      this.userService.getPastOrders()
        .subscribe(
        past => {
          this.gs.pastorders = past;
          console.log("gs past: ", this.gs.pastorders)
        },
        error => {
          this.alertService.error(error);
        })

        this.userService.getCurrentOrders()
        .subscribe(
        current => {
          this.gs.currentorders = current;
          console.log("gs current: ", this.gs.currentorders)
        },
        error => {
          this.alertService.error(error);
        })
    }

    ngOnInit(): void {
      this.getAllOrder();
    }
}

