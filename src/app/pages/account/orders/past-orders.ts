import { Component, OnInit } from '@angular/core';
import { Orders } from '../../place-order/order';
import { UserService } from '../../../shared/_services/user.service';
import { generalService } from '../../homepage/general.service';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'past-orders',
  templateUrl: './past-orders.html',
  styleUrls: ['./orders.scss'],
})
export class PastOrdersComponent implements OnInit {

  constructor(public router: Router,
    private gs: generalService) { }

  orders = this.gs.pastorders;
  pastOrders = new Orders('','','')
  items = [];

  renavigate(){

    this.router.navigate(["/"]);
  }
  ngOnInit() {
    if(this.gs.pastorders){
    for (var i = 0; i < this.gs.pastorders.length; ++i) {
      var obj = this.gs.pastorders[i];
      this.pastOrders.name = obj.eventname;
      this.pastOrders.number = this.gs.getNumberString(i+1);
      this.pastOrders.id = obj.id;
      this.items[i] = this.pastOrders;
    }
  }
  }
}
