import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {SvgViewerModule} from '../../shared/svg-viewer/svg-viewer';
import {MatButtonModule, MatTabsModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterModule} from '../../shared/footer/footer';
import {RouterModule} from '@angular/router';
import {ComponentPageTitle} from '../page-title/page-title';
import { UserService } from '../../shared/_services/user.service';
import { AlertService } from '../../shared/_services/alert.service';
import { generalService } from '../homepage/general.service';
import {Router, NavigationEnd} from '@angular/router';


@Component({
  selector: 'account',
  templateUrl: './account.html',
  styleUrls: ['./account.scss'],
})
export class Account implements OnInit {

  constructor(public router: Router,
    private userService: UserService,
    private alertService: AlertService,
  private gs: generalService){}
  getAllOrder(): void {

      this.userService.getPastOrders()
        .subscribe(
        past => {
          this.gs.pastorders = past;
          console.log("gs past: ", past)
        },
        error => {
          this.alertService.error(error);
        })

        this.userService.getCurrentOrders()
        .subscribe(
        current => {
          this.gs.currentorders = current;
          console.log("gs current: ", current)
        },
        error => {
          this.alertService.error(error);
        })
    }

    ngOnInit(){
         this.getAllOrder();
         if(this.router.url != "/order"){
          this.gs.showLara = false;
        }
    }
}
