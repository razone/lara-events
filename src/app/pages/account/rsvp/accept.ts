import {Component, OnInit} from '@angular/core';
import { RSVP } from '../../place-order/order'

@Component({
    selector: 'accept-rsvp',
    templateUrl: './accept.html',
  })
  export class AcceptComponent implements OnInit {
  
    items: RSVP[] = [
        { name: "Wunmi Adebayo", number: "one"},
        { name: "James Okufi", number:"two"},
        { name: "Ore Adebanjo", number: "3"},
        { name: "Ngozi Alade", number: "4"},
        { name: "Segun", number: "5"},
        { name: "Vincent Marcus", number:"6"},
        { name: "Folake Koro", number: "seven"},
        { name: "Gregory Mcoye", number: "eight"}
          ]

      ngOnInit(){
          
      }
  }