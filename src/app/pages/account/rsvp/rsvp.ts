import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {SvgViewerModule} from '../../../shared/svg-viewer/svg-viewer';
import {MatButtonModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterModule} from '../../../shared/footer/footer';
import {RouterModule} from '@angular/router';
import {ComponentPageTitle} from '../../page-title/page-title';

@Component({
  selector: 'account-rsvp',
  templateUrl: './rsvp.html',
  styleUrls: ['./rsvp.scss'],
})
export class RSVPComponent implements OnInit {

    ngOnInit(){
        
    }
}