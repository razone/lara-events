import {Component, OnInit} from '@angular/core';
import { RSVP } from '../../place-order/order'

@Component({
    selector: 'decline-rsvp',
    templateUrl: './decline.html',
  })
  export class DeclineComponent implements OnInit {
  
    items: RSVP[] = [
        { name: "Bolanle Adebayo", number: "one"},
        { name: "James Okufi", number:"two"},
        { name: "Fatima Adebanjo", number: "3"},
        { name: "Ngozi Alade", number: "4"}
          ]
      ngOnInit(){
          
      }
  }