import {Component, NgModule, OnInit, ViewChild, Output,EventEmitter} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {SvgViewerModule} from '../../shared/svg-viewer/svg-viewer';
import {MatButtonModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterModule} from '../../shared/footer/footer';
import { QRCodeModule } from 'angular2-qrcode';
import {RouterModule} from '@angular/router';
import {ComponentPageTitle} from '../page-title/page-title';
import { MatSelectModule, MatProgressBarModule, MatListModule, MatStepper, MatStepperModule, MatIconModule, MatNativeDateModule, MatDialogModule, MatDatepickerModule, MatRadioModule, MatTableModule, MatIconRegistry, MatCheckboxModule, MatDialog, MatGridListModule, MatInputModule, MatMenuModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MilestonesModule } from '../milestones/milestones';
import {PlaceOrderModule} from '../place-order/place-order';
import {PlaceOrderViewModule} from '../place-order/place-order-view';
import { CategoryService } from '../place-order/category.service';
import {SignUpDialogModule} from '../sign-up/sign-up';
import {TrackProgressModule} from '../track-progress/track-progress';
import {GooglePlaceModule} from "angular2-google-place";
import {PlaceOrderService} from '../place-order/place-order.service'
import { generalService } from '../homepage/general.service';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { CustomValidators } from 'ng2-validation';
import { FileUploader, FileUploadModule, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import 'rxjs/add/operator/toPromise';
import { Cloudinary } from '@cloudinary/angular-4.x';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.html',
  styleUrls: ['./homepage.scss'],
})
export class Homepage implements OnInit {
  constructor(public _componentPageTitle: ComponentPageTitle, private _iconRegistry: MatIconRegistry,
              private _sanitizer: DomSanitizer, public dialog: MatDialog,

              private gs: generalService) {}

 public campaign = true;
    public loggedIn = this.gs.loggedIn;
    formResult: string[];


   openDialog(): void {
    let dialogRef = this.dialog.open(SignUpDialogModule, {
      height: '550px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.formResult = result;
      console.log(this.formResult);
    });
  }

  ngOnInit(): void {
    this._componentPageTitle.title = '';
    this._iconRegistry
      .addSvgIcon('step-done', this._sanitizer.bypassSecurityTrustResourceUrl('assets/icon/done.svg'));
    this._iconRegistry
      .addSvgIcon('step-warning', this._sanitizer.bypassSecurityTrustResourceUrl('assets/icon/warning.svg'));


  }

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  // @ViewChild('stepperDemo')
  // public steppers: NgxStepperComponent;

   @Output()
  accountDetail = new EventEmitter();


   previousStep(stepper: MatStepper) {
    stepper.previous();
  }

 nextStep(stepper: MatStepper) {
  stepper.next();
}

}


export class HomepageModule {}
