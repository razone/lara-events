import { Injectable } from "@angular/core";
import { Order, dateArray, addressArray } from "../place-order/order";
import { Element } from "../milestones/milestones";
@Injectable()
export class generalService {
  loggedIn: boolean = false;
  isAdmin: boolean = false;
  account: any;
  accountName: string;
  user: any;
  showLara: boolean = false;
  orders: String[];
  currentEventName: String;
  orderCreatedDate: Date;
  defaultDate: dateArray;
  defaultAddress: addressArray;
  order: Order = new Order(
    "",
    "",
    "",
    "",
    [this.defaultDate],
    [this.defaultAddress],
    "",
    "",
    ""
  );
  currentorder: any;
  currentorders: Order[];
  pastorders: Order[];
  appconfig: string;
  role: number;
  numberOfDaysToAdd = 7;
  orderDate = new Date();
  deliveryDate: Date = new Date();
  orderStatus;

  date: Date = new Date();
  saveOrders(r: any) {
    this.orders = JSON.parse(r);
  }
  toogleLoggedIn() {
    this.loggedIn = !this.loggedIn;
    if (this.loggedIn) {
      var res = localStorage.getItem("laraCurrentUser");
      this.account = JSON.parse(res).user;
      this.user = JSON.parse(res);
      this.role = JSON.parse(res).role;
      if (this.role < 2) {
        this.isAdmin = true;
      }
      else{
        this.isAdmin = false;
      }
    } else {
      localStorage.removeItem("laraCurrentUser");
    }
    // this.zone.runOutsideAngular(() => {
    //     location.reload();
    // });
    // location.reload();
  }

  data: Element[] = [
    {
      description: "Mobilization Fee", amount: "N15, 000", status: "", action: "H", color: "", date: this.date, disable: true
    },
    {
      description: "Final Deliverable Fee",
      amount: "N35, 000",
      status: "",
      action: "He",
      color: "",
      date: this.deliveryDate,
      disable: true
    }
  ];

  setConfig(s: string) {
    this.appconfig = s;
    console.log("appconfig: ", this.appconfig);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("laraCurrentUser");
  }

  getNumberString(i: Number): string {
    var num: string;

    switch (i) {
      case 1:
        return (num = "one");
      case 2:
        return (num = "two");
      case 3:
        return (num = "three");
      case 4:
        return (num = "four");
      case 5:
        return (num = "five");
      case 6:
        return (num = "six");
      case 7:
        return (num = "seven");
      case 8:
        return (num = "eight");
      case 9:
        return (num = "nine");
      default:
        return (num = "");
    }
  }

  style = [
    {
      featureType: "all",
      elementType: "geometry",
      stylers: [
        {
          color: "#9fadb5"
        }
      ]
    },
    {
      featureType: "all",
      elementType: "labels.text.fill",
      stylers: [
        {
          gamma: 0.01
        },
        {
          lightness: 20
        }
      ]
    },
    {
      featureType: "all",
      elementType: "labels.text.stroke",
      stylers: [
        {
          saturation: -31
        },
        {
          lightness: -33
        },
        {
          weight: 2
        },
        {
          gamma: 0.8
        }
      ]
    },
    {
      featureType: "all",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "landscape",
      elementType: "geometry",
      stylers: [
        {
          lightness: 30
        },
        {
          saturation: 30
        }
      ]
    },
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [
        {
          saturation: 20
        }
      ]
    },
    {
      featureType: "poi.attraction",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "geometry",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.government",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.medical",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.medical",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [
        {
          lightness: 20
        },
        {
          saturation: -20
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "geometry.fill",
      stylers: [
        {
          visibility: "on"
        },
        {
          color: "#d3e3ac"
        }
      ]
    },
    {
      featureType: "poi.place_of_worship",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.school",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.school",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "poi.sports_complex",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [
        {
          lightness: 10
        },
        {
          saturation: -30
        }
      ]
    },
    {
      featureType: "road",
      elementType: "geometry.stroke",
      stylers: [
        {
          saturation: 25
        },
        {
          lightness: 25
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        {
          visibility: "on"
        },
        {
          color: "#ffffff"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [
        {
          color: "#d9e021"
        },
        {
          weight: "0.62"
        },
        {
          invert_lightness: true
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "road.highway.controlled_access",
      elementType: "geometry.fill",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "road.arterial",
      elementType: "geometry.fill",
      stylers: [
        {
          color: "#dad7c6"
        }
      ]
    },
    {
      featureType: "transit",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "transit",
      elementType: "labels.icon",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "transit.station.airport",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "all",
      stylers: [
        {
          lightness: -20
        }
      ]
    }
  ];
}
