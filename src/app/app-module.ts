import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { MatNativeDateModule, MatDatepickerModule, MatSortModule } from '@angular/material';
import { QRCodeModule } from 'angular2-qrcode';
import { GooglePlaceModule } from "angular2-google-place"
import { MatButtonModule, MatProgressBarModule, MatStepperModule, MatMenuModule, MatListModule, MatTabsModule, MatInputModule, MatDialogModule, MatSelectModule, MatRadioModule, MatOptionModule, MatToolbarModule, MatCheckboxModule } from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import { MaterialDocsApp } from './material-docs-app';
import { HomepageModule } from './pages/homepage/homepage';
import { MainAppModule } from './material-docs-app';
import { PlaceOrderModule } from './pages/place-order/place-order';
import { MilestonesModule } from './pages/milestones/milestones';
import { Account } from './pages/account/account';
import { MATERIAL_DOCS_ROUTES } from './routes';
import { FooterModule } from './shared/footer/footer';
import { ComponentPageTitle } from './pages/page-title/page-title';
import { StyleManager } from './shared/style-manager/style-manager';
import { SvgViewerModule } from './shared/svg-viewer/svg-viewer';
import { ThemePickerModule } from './shared/theme-picker/theme-picker';
import { ThemeStorage } from './shared/theme-picker/theme-storage/theme-storage';
import { SignUpDialogModule } from './pages/sign-up/sign-up';
import { LoginDialogModule } from './pages/sign-up/login';
import {AdminViewModule} from './pages/admin/admin-view';
import {DeleteDialog} from './pages/admin/admin-view-order'
import { AuthGuard } from './shared/_guard/index';
import {AlertComponent} from './shared/_directives';
import { AlertService, } from './shared/_services/alert.service';
import { UserService } from './shared/_services/user.service';
import { AuthenticationService } from './shared/_services/authentication.service';
import {OrdersComponent} from './pages/account/orders/orders';
import {CurrentOrdersComponent} from './pages/account/orders/current-orders';
import {PastOrdersComponent} from './pages/account/orders/past-orders';
import {RSVPComponent} from './pages/account/rsvp/rsvp';
import {AcceptComponent} from './pages/account/rsvp/accept';
import {DeclineComponent} from './pages/account/rsvp/decline';
import {AdminViewOrderModule} from './pages/admin/admin-view-order';
import {MATERIAL_COMPATIBILITY_MODE} from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { Angular4PaystackModule } from 'angular4-paystack';// File upload module
import {FileUploadModule} from 'ng2-file-upload';
import { CustomValidators } from 'ng2-validation';
import {MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { MatPaginatorModule } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import {PlaceOrderViewModule} from './pages/place-order/place-order-view';
import { DisableControlDirective } from './shared/_directives/DisableDirective';

// Cloudinary module

import { Cloudinary } from 'cloudinary-core';
import {CloudinaryModule, CloudinaryConfiguration, provideCloudinary} from '@cloudinary/angular-4.x';
import cloudinaryConfiguration from './config';
import * as cloudinary from 'cloudinary-core';

export const cloudinaryLib = {
  Cloudinary: Cloudinary
};

@NgModule({
  imports: [
    CdkTableModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    Angular4PaystackModule,
    HttpModule,
    MatNativeDateModule,
    MatRadioModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatDialogModule,
    FlexLayoutModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatStepperModule,
    MatOptionModule,
    MatTabsModule,
    MatToolbarModule,
    CloudinaryModule.forRoot(cloudinaryLib, {cloudinary, cloudinaryConfiguration}),
    FileUploadModule,
    RouterModule.forRoot(MATERIAL_DOCS_ROUTES),
    QRCodeModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    GooglePlaceModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCFzSnR2kfsJewMAlri39zjmzQ1ceIUDAQ",
      libraries: ["places"]
    }),

    FooterModule,
    MainAppModule,
    SvgViewerModule,
    ThemePickerModule,
  ],
  declarations: [SignUpDialogModule, LoginDialogModule, Account],
  entryComponents: [SignUpDialogModule, LoginDialogModule, DeleteDialog],
  providers: [
    ComponentPageTitle,
    StyleManager,
    ThemeStorage,
    AuthGuard,
    AlertComponent,
    AlertService,
    AuthenticationService,
    UserService,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    {provide: MATERIAL_COMPATIBILITY_MODE, useValue: true},
  ],
  bootstrap: [MaterialDocsApp],
})
export class AppModule { }
