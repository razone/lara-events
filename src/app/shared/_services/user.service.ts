import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Cloudinary} from '@cloudinary/angular-4.x';

import { User, Order, Login } from '../../pages/place-order/order';

export class Photo {
    public_id: string;
    context: any;
  }


@Injectable()
export class UserService {

    private registerUrl = '/api/customer/register';
    private orderUrl = '/api/orders/register';
    private infoUrl = '/api/orders/info';
    private updateOrderUrl = '/api/orders/update?id=';
    private loginUrl = '/api/customer/authenticate';
    private configUrl = '/api/config';
    private deleteOrderUrl = '/api/orders/delete?id=';
    constructor(private http: Http,private cloudinary: Cloudinary) { }


    getAllOrders() {
        return this.http.get('/api/orders/all', this.jwt()).map((response: Response) => response.json());
    }

    getOrders() {
        return this.http.get('/api/orders/all/pages', this.jwt()).map((response: Response) => response.json());
    }

    getConfig() {
        return this.http.get(this.configUrl, this.jwt()).map((response: Response) => response.json());
    }

    getUser(id: String) {
        return this.http.get('/api/user?id='+id, this.jwt()).map((response: Response) => response.json());
    }

    getCurrentOrders() {
        return this.http.get('/api/orders/all/currentorders', this.jwt()).map((response: Response) => response.json());
    }

    getPastOrders() {
        return this.http.get('/api/orders/all/pastorders', this.jwt()).map((response: Response) => response.json());
    }

    getLatestOrder() {
        return this.http.get('/api/orders/latest', this.jwt()).map((response: Response) => response.json());
    }

    getByEmail(email: string) {
        return this.http.get('/api/users/' + email, this.jwt()).map((response: Response) => response.json());
    }

    getOrdersInfo() {
        return this.http.get(this.infoUrl, this.jwt()).map((response: Response) => response.json());
    }

    create(user: User) {
        return this.http.post('/api/users', user, this.jwt())
            .map((response: Response) => response.json());
    }

    updateOrder(order: Order,id: string) {
        return this.http.post(this.updateOrderUrl+id, order, this.jwt())
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any;
    }


    deleteOrder(id: string) {
      return this.http.post(this.deleteOrderUrl+id, this.jwt())
          .map((response: Response) => response.json())
          .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any;
  }

    addUser(body: Object): Observable<User> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.registerUrl, body, options) // ...using post request
            .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    addOrder(body: Object): Observable<Order> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.orderUrl, body, options) // ...using post request
            .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    login(body: Object): Observable<Login> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.loginUrl, body, options) // ...using post request
            .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }


    getPhotos(): Observable<Photo[]> {
        // instead of maintaining the list of images, we rely on the 'myphotoalbum' tag
        // and simply retrieve a list of all images with that tag.
        let url = this.cloudinary.url('myphotoalbum', {
            format: 'json',
            type: 'list',
            // cache bust (lists are cached by the CDN for 1 minute)
            // *************************************************************************
            // Note that this is practice is DISCOURAGED in production code and is here
            // for demonstration purposes only
            // *************************************************************************
            version: Math.ceil(new Date().getTime() / 1000)
        });

        return this.http
            .get(url)
            .map((r: Response) => r.json().resources as Photo[]);
    }

    // update(user: User) {
    //     return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    // }

    // delete(id: number) {
    //     return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    // }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('laraCurrentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}
