import {Component, ViewEncapsulation, OnInit, NgModule} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {SvgViewerModule} from './shared/svg-viewer/svg-viewer';
import {Router, NavigationEnd} from '@angular/router';
import {FooterModule} from './shared/footer/footer';
import { QRCodeModule } from 'angular2-qrcode';
import {RouterModule} from '@angular/router';
import {MatButtonModule, MatPaginatorModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatSelectModule, MatProgressBarModule, MatStepperModule, MatListModule, MatTabsModule, MatIconModule, MatNativeDateModule, MatDialogModule, MatDatepickerModule, MatRadioModule, MatTableModule, MatIconRegistry, MatCheckboxModule, MatDialog, MatGridListModule, MatInputModule, MatMenuModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MilestonesModule } from './pages/milestones/milestones';
import {PlaceOrderModule} from './pages/place-order/place-order';
import {Homepage} from './pages/homepage'
import { CategoryService } from './pages/place-order/category.service';
import {SignUpDialogModule} from './pages/sign-up/sign-up';
import { LoginDialogModule } from './pages/sign-up/login';
import {AdminViewModule, AdminViewDataSource, AdminViewHttpDao} from './pages/admin/admin-view';
import {AdminViewOrderModule} from './pages/admin/admin-view-order';
import {PlaceOrderViewModule} from './pages/place-order/place-order-view';
import {DeleteDialog} from './pages/admin/admin-view-order'
import {TrackProgressModule} from './pages/track-progress/track-progress';
import {GooglePlaceModule} from "angular2-google-place"
import {PlaceOrderService} from './pages/place-order/place-order.service'
import {OrdersComponent} from './pages/account/orders/orders';
import {RSVPComponent} from './pages/account/rsvp/rsvp';
import { generalService } from './pages/homepage/general.service'
import {CurrentOrdersComponent} from './pages/account/orders/current-orders';
import {PastOrdersComponent} from './pages/account/orders/past-orders';
import {AcceptComponent} from './pages/account/rsvp/accept';
import {DeclineComponent} from './pages/account/rsvp/decline';
import { AlertService } from './shared/_services/alert.service';
import { AlertComponent } from './shared/_directives';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { AgmCoreModule } from '@agm/core';
import { Angular4PaystackModule } from 'angular4-paystack';
import { UserService } from './shared/_services/user.service';
import { FileUploader, FileUploadModule, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import 'rxjs/add/operator/toPromise';
import { Cloudinary } from '@cloudinary/angular-4.x';
import { CustomValidators } from 'ng2-validation';
import {MatPaginator, MatSortModule} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { FlexLayoutModule } from "@angular/flex-layout";
import { DisableControlDirective } from './shared/_directives/DisableDirective';
import 'rxjs/add/operator/filter';


@Component({
  selector: 'material-docs-app',
  templateUrl: './material-docs-app.html',
  styleUrls: ['./material-docs-app.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MaterialDocsApp implements OnInit{
  showShadow = false;

  public account: any;
  public loggedIn = this.gs.loggedIn;
  public appconfig: any;

    navButton = function () {
        this.router.navigateByUrl('/account');
};

adminButton = function () {
  this.router.navigateByUrl('/admin');
};

logout(): void {
  this.gs.logout();
  this.gs.toogleLoggedIn();
  this.router.navigate(['/']);
  console.log("logged out from home");
  this.alertService.success('Logout successful', true);
}
openLoginDialog(): void {
  let dialogRef = this.dialog.open(LoginDialogModule, {

    height: '350px',
    data: {}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
   // this.orderFinished.emit()
  });
}

 openLaraTab(hashtag): void {
  var win = window.open('https://www.lara.ng/'+hashtag, '_blank');
  win.focus();
}

openDialog(): void {
  let dialogRef = this.dialog.open(SignUpDialogModule, {

    height: '550px',
    data: {}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    //this.formResult = result;
    //console.log(this.formResult);
    //this.selectCampaign();
  });
}

getApplicationConfig(): void {
  this.userService.getConfig()
    .subscribe(
    data => {
      this.gs.setConfig(JSON.stringify(data));
    },
    error => {
      this.alertService.error(error);
    });
  }


ngOnInit(): void{
  if(this.router.url != "/order"){
    this.gs.showLara = false;
  }
  if (localStorage.getItem('laraCurrentUser')) {
    // logged in so return true
  this.gs.toogleLoggedIn();
  this.getApplicationConfig();
  //this.account = localStorage.getItem('laraCurrentUser');
}
}

  constructor(public router: Router, public gs: generalService, public dialog: MatDialog,private alertService: AlertService,
    private userService: UserService, private cloudinary: Cloudinary) {
    // const routesWithNavbarShadow = ['/categories', '/components'];

    // let previousRoute = router.routerState.snapshot.url;

    // router.events
    //   .filter(event => event instanceof NavigationEnd )
    //   .subscribe((data: NavigationEnd) => {
    //     this.showShadow = !!routesWithNavbarShadow
    //         .find(route => data.urlAfterRedirects.startsWith(route));

    //     // We want to reset the scroll position on navigation except when navigating within
    //     // the documentation for a single component.
    //     if (!isNavigationWithinComponentView(previousRoute, data.urlAfterRedirects)) {
    //       resetScrollPosition();
    //     }

    //     previousRoute = data.urlAfterRedirects;
      // });


  }


}

@NgModule({
  imports: [SvgViewerModule, FormsModule, MatPaginatorModule, FlexLayoutModule, FileUploadModule, Angular4PaystackModule, ReactiveFormsModule, MatStepperModule, MatListModule, MatProgressBarModule, GooglePlaceModule, MatNativeDateModule, QRCodeModule, MatIconModule, MatDialogModule, MatTableModule, CommonModule, MatInputModule, MatGridListModule, MatRadioModule, MatMenuModule, MatCheckboxModule, MatButtonModule, MatDatepickerModule,MatSortModule, MatSelectModule, FooterModule, RouterModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCFzSnR2kfsJewMAlri39zjmzQ1ceIUDAQ",
      libraries: ["places"]
    })],
  exports: [MaterialDocsApp],
  declarations: [MaterialDocsApp, PlaceOrderModule, DisableControlDirective, PlaceOrderViewModule, Homepage, AlertComponent,DeleteDialog, AdminViewModule, AdminViewOrderModule, MilestonesModule, TrackProgressModule,OrdersComponent, RSVPComponent, CurrentOrdersComponent, PastOrdersComponent, AcceptComponent, DeclineComponent],
  providers: [CategoryService, PlaceOrderService, generalService],
})

export class MainAppModule{}

function isNavigationWithinComponentView(oldUrl: string, newUrl: string) {
  const componentViewExpression = /components\/(\w+)/;
  return oldUrl && newUrl
      && componentViewExpression.test(oldUrl)
      && componentViewExpression.test(newUrl)
      && oldUrl.match(componentViewExpression)[1] === newUrl.match(componentViewExpression)[1];
}

function resetScrollPosition() {
  if (typeof document === 'object' && document) {
    const sidenavContent = document.querySelector('.mat-sidenav-content');
    if (sidenavContent) {
      sidenavContent.scrollTop = 0;
    }
  }
}
