// Get passport
const passport = require('./middleware/passport');

// Routes
const apiRouter = require('./routes/api/apiRoutes');
const adminRouter = require('./routes/admin');
const authRouter = require('./routes/auth');
const orderRouter = require('./routes/orders');
//const overrideRouter = require('./routes/override');
const rsvpRouter = require('./routes/rsvp');
const userRouter = require('./routes/customer');

module.exports = function(app) {
    // server routes
    app.use('/api', apiRouter);
    app.use('/api/auth', authRouter);
    app.use('/api/admin', adminRouter);
    app.use('/api/rsvp', rsvpRouter);
    app.use('/api/customer', userRouter);
    app.use('/api/orders', orderRouter);
 //   app.use('/override', overrideRouter);

    // frontend routers
    
    // Sign Up Routes
    app.get('/api/signup', function(req, res) {
        // load the index page
        res.sendfile('./public/views/signup.html', {message: req.flash('SignUpMessage')});
    });
    
    // app.get('/homepage', isLoggedIn, function(req, res) {
    //     // load the home page
    //     res.sendfile('./public/views/base.html');
    // });

    // app.get('/merchant/login', function(req, res) {
    //     //load the merchant login page
    //     res.sendfile('./public/views/merchantReg.html');
    // });

    app.get('/api/logout', function(req, res) {
        req.logout();
        res.redirect('/admin');
    });

    // frontend routers
    // routes to handle all angular requests
    // app.get('*', function(req, res) {
    //     // load the index page
    //     res.sendfile('./public/views/index.html');
    // });
    // route middleware to ensure user is logged in
    function isLoggedIn(req, res, next) {
        // if user is logged in then carry frontend
        if(req.isAuthenticated()) {
            return next();
        }

        // if not redirect to login screen
        res.redirect('/admin');
    }
}