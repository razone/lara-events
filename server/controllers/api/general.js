// models
const Config = require('./../../models/applicationConfig');
module.exports = {

    getAll: function (req, res) {
        Config.find({}, function(err, docs) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(docs);
            }
        });
    }
};