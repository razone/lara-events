const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');
var mongoose = require('mongoose');
//var ObjectId = require('mongodb').ObjectID;

const Orders = require('./../../models/orders');
const User = require('./../../models/users')
//const emailer = require('../../config/email');

// Coupin App Messages
//const messages = require('../../config/messages');

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'appcustomer'
}

module.exports = {

    getOrderDirections: function (req, res) {
        Orders.getOrderByName(req.query.eventname, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            }  else if(!orders){
                res.status(404).send({message: "There are no orders"});
        }
        else {

                res.status(200).send({
                    location: orders.addresses,
                    iconUrl: orders.banner,
                    videoUrl: orders.videoUrl
                });
            };
        })
    },

    getOrderIntro: function (req, res) {
        Orders.getOrderByName(req.query.eventname, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } 
            else if(!orders){
                res.status(404).send({message: "There are no orders"});
        }
        else {
                res.status(200).send({
                    type: orders.category,
                    location: orders.addresses,
                    evendDate: orders.dates.date,
                    host: orders.host,
                    screens: {screenImageUrl: orders.banner, screenCTA: orders.callToActionDetail, rsvpDate: orders.rsvpdate},
                    menu: {menuIconUrl: orders.typeUrl, menuCTA: orders.callToAction}
                });
            }
        ;
    })
},
getUser: function(req, res){
    var id = String(req.query.id)
    
    User.findById(id, function(err,user){
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send({
                name: user.firstname + ' ' + user.lastname,
                mobile: user.mobileNumber,
                email: user.email
            });
        }
    })
}
};