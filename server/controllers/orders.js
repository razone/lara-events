const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');

const Orders = require('../models/orders');
const User = require('../models/users');
//const emailer = require('../../config/email');

// Coupin App Messages
//const messages = require('../../config/messages');

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'appcustomer'
}

module.exports = {
    adminCreate: function (req, res) {
        const body = req.body;

        Merchant.findOne({ email: body.email }, function (err, merchant) {
            if (err) {
                res.status(500).send(err);
            } else if (merchant) {
                res.status(409).send({ message: 'User already exists' });
            } else {
                let merchant = new Merchant({
                    email: body.email,
                    picture: body.picture || null,
                    password: body.password || 'merchant',
                    merchantInfo: {
                        companyName: body.companyName,
                        companyDetails: body.companyDetails,
                        address: body.address,
                        city: body.city,
                        mobileNumber: body.mobileNumber,
                        location: {
                            lat: body.latitude || null,
                            long: body.longitude || null
                        }
                    },
                    isActive: true,
                    activated: true,
                    role: 2
                });

                Merchant.createCustomer(merchant, function (err) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        res.status(200).send({ message: 'User created successfully.' });
                    }
                });
            }
        });
    },
    authenticate: function (req, res) {
        req.logIn(req.user, function (err, user) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send({ success: true, user: user });
            }
        });
    },
    confirm: function (req, res) {
        // get the data from the the
        const address = req.body.address;
        const city = req.body.city;
        const password = req.body.password;
        const state = req.body.state;

        // Form Validator
        req.checkBody('address', 'Address field is required').notEmpty();
        req.checkBody('password', 'Password field is required').notEmpty();
        req.checkBody('password2', 'Please confirm password').notEmpty();
        req.checkBody('password2', 'Passwords are not the same').equals(req.body.password);
        req.checkBody('city', 'City field is required').notEmpty();
        req.checkBody('state', 'State field is required').notEmpty();

        const errors = req.validationErrors();

        if (errors) {
            res.send({ success: false, message: errors[0].msg });
        } else {
            Merchant.findById(req.params.id, function (err, merchant) {
                if (err)
                    throw err;

                merchant.merchantInfo.address = address;
                merchant.password = password;
                merchant.merchantInfo.city = city;
                merchant.merchantInfo.state = state;
                merchant.activated = true;
                merchant.isPending = false;
                merchant.rejected = false;

                Merchant.createCustomer(merchant, function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }

                    res.status(200).send({ success: true, message: 'You have been confirmed!' });
                });
            });
        }
    },
    deleteOne: function (req, res) {
        Merchant.findByIdAndRemove(req.params.id, function (err, merchant) {
            if (err)
                throw err;

            res.send({ message: 'Merchant Deleted' });
        });
    },
    getAllOrders: function (req, res) {
        Orders.getOrderByUserId(req.user.id, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(orders);
            }
        });
    },
    getOrders: function (req, res) {

        var st = req.query.sort;
        var qt = req.query.order;
        var lt = Number(req.query.limit);
        var options = {
            sort: { st: qt },
            page: req.query.page,
            limit: lt
        }
        Orders.paginate({deletedOrder: false}, options).then(function (result) {
            // if (err) {
            //     res.status(500).send(err);
            // } else {
            res.status(200).send({
                orders: result.docs,
                total_count: result.total
            });
            // }
        });
        // Orders.getOrder(function (err, orders) {
        //     if (err) {
        //         res.status(500).send(err);
        //     } else {
        //         res.status(200).send({
        //             orders: orders,
        //             total_count: orders.length
        //         });
        //     }
        // });
    },
    getCurrentOrders: function (req, res) {
        Orders.getCurrentOrderByUserId(req.user.id, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(orders);
            }
        });
    },
    getPastOrders: function (req, res) {
        Orders.getPastOrderByUserId(req.user.id, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(orders);
            }
        });
    },
    getLatestOrder: function (req, res) {
        Orders.getLatestOrder(req.user.id, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(orders);
            }
        });
    },
    getOne: function (req, res) {
        Orders.getOrderByName(req.query.eventname, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } else {

                res.json(order);
            }
        })
    },
    getOrdersData: function (req, res) {
        Orders.find().exec({ status: 'Done', deletedOrder: false }, function (err, results) {
            var doneOrders = results.length
            if (err) {
                res.status(500).send(error);
            } else {
                Orders.find({ status: 'Processing', deletedOrder: false }, function (err, results) {
                    var processingOrders = results.length
                    if (err) {
                        res.status(500).send(error);
                    } else {
                        Orders.find({ status: 'Review', deletedOrder: false }, function (err, results) {
                            var reviewOrders = results.length
                            if (err) {
                                res.status(500).send(error);
                            } else {
                                Orders.find({deletedOrder: false},function (err, results) {
                                    var ordersCount = results.length

                                    if (err) {
                                        res.status(500).send(error);
                                    } else {
                                        res.status(200).send({
                                            doneCount: doneOrders,
                                            processingCount: processingOrders,
                                            reviewCount: reviewOrders,
                                            totalCount: ordersCount
                                        });
                                    }
                                })

                            }
                        });
                    }
                });
            }
        });

    },
    register: function (req, res) {
        // Get order details
        const eventname = req.body.eventname;
        const category = req.body.category;
        const host = req.body.host;
        const dates = req.body.dates;
        // const dateLabel = req.body.dates.label;
        // const date = req.body.dates.date;
        // const rsvpdate = req.body.dates.rsvpdate;
        const addresses = req.body.addresses;
        //    const address = req.body.addresses.address;
        //    const addressLongitude = req.body.addresses.addressLongitude;
        //    const addressLatitude = req.body.addresses.addressLatitude;
        //    const addressLabel = req.body.addresses.label;
        const callToAction = req.body.callToAction;
        const banner = req.body.banner;
        const userId = req.body.userId;
        const callToActionDetail = req.body.callToActionDetail;
        const type = req.body.type;
        const typeUrl = req.body.typeUrl;
        const ctaUrl = req.body.ctaUrl;


        // Form Validator
        req.checkBody('eventname', 'Event Name field is required').notEmpty();
        req.checkBody('category', 'Category field is required').notEmpty();
        req.checkBody('host', 'Host field cannot be empty').notEmpty();
        req.checkBody('dates', 'Date field is required').notEmpty();
        req.checkBody('addresses', 'Addresses field is required').notEmpty();
        req.checkBody('callToAction', 'Call to action field cannot be empty').notEmpty();
        req.checkBody('userId', 'User Id field cannot be empty').notEmpty();
        // req.checkBody('addressLongitude','Address field is required').notEmpty();
        // req.checkBody('addressLatitude','Address field is required').notEmpty();
        req.checkBody('type', 'Category type field is required').notEmpty();
        req.checkBody('typeUrl', 'Category type url field is required').notEmpty();

        // Check Errors
        const errors = req.validationErrors();

        if (errors) {
            res.status(400).send({ success: false, message: errors[0].msg });
        } else {
            var createdDate = Date.now();
            var orders = new Orders();      // create a new instance of the Customer model
            orders.eventname = eventname;
            orders.category = category;
            orders.type = type;
            orders.typeUrl = typeUrl;
            orders.host = host;
            orders.dates = dates;
            orders.ctaUrl = ctaUrl;
            // orders.dates.label = dateLabel;
            // orders.dates.date = date;
            // orders.dates.rsvpdate = rsvpdate;
            orders.createdDate = createdDate;
            orders.addresses = addresses;
            // orders.addresses.address = address;
            // orders.addresses.addressLongitude = addressLongitude;
            // orders.addresses.addressLatitude = addressLatitude;
            // orders.addresses.label = addressLabel;
            orders.callToAction = callToAction;
            orders.banner = banner
            orders.userId = userId;
            // orders.addressLatitude = addressLatitude;
            // orders.addressLongitude = addressLongitude;
            orders.callToActionDetail = callToActionDetail;

            orders.save(function (err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send({
                        success: true,
                        message: 'Success! Your request has now been made and we will get back to you within 24hours.'
                    });
                };
            });
        }
    },
    search: function (req, res) {
        const query = req.params.query;

        Merchant.find({
            'merchantInfo.companyName':
                {
                    '$regex': query,
                    '$options': 'i'
                },
            role: 2
        }, function (err, merchants) {
            if (err) {
                res.status(500).send(error);
            } else if (merchants.length === 0) {
                res.status(404).send({ message: 'No Merchants under that name was found' });
            } else {
                res.status(200).send(merchants);
            }
        });
    },
    update: function (req, res) {
        var id = String(req.query.id)
        Orders.findById(id, function (err, order) {
            if (err) {
                res.status(500).send(err);
            } else if (!order) {
                res.status(404).send({ message: 'No such order exists' });
            } else {
                if (req.body.status) {
                    order.status = req.body.status;
                }

                if (req.body.deletedOrder) {
                    order.deletedOrder = req.body.deletedOrder;
                }

                if (req.body.videoUrl) {
                    order.videoUrl = req.body.videoUrl;
                }

                if (req.body.eventname) {
                    order.eventname = req.body.eventname;
                }

                if (req.body.host) {
                    order.host = req.body.host;
                }

                if (req.body.category) {
                    order.category = req.body.category;
                }

                if (req.body.type) {
                  order.type = req.body.type;
              }

                // if (req.body.merchantInfo.state) {
                //     merchant.merchantInfo.state = req.body.merchantInfo.state;
                // }

                // if (req.body.merchantInfo.location) {
                //     merchant.merchantInfo.location = req.body.merchantInfo.location;
                // }

                order.modifiedDate = Date.now();

                // save the customer updateCustomer
                order.save(function (err) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        res.status(200).send({ message: 'Order updated!' });
                    }

                });
            }
        });
    }
}
