const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');

const Customer = require('../models/users');
const emailer = require('../config/email');

// Coupin App Messages
const messages = require('../config/messages');

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'appcustomer'
}

module.exports = {
    login: function (req, res) {
        var customer = req.user;
        var payload = { id: customer.id, name: customer.name, email: customer.email };
        var token = jwt.sign(payload, jwtOptions.secretOrKey);

        //var token = jwt.sign(customer, secretKey, {
        //  expiresInMinutes: 1440
        //});

        res.json({
            success: true,
            token: 'JWT ' + token
        });
    },
    authenticate: function (req, res) {
        req.logIn(req.user, function (err, user) {
            if (err) {
                res.status(500).send(err);
            } else {
                
                var cust = Customer.findOne({
                    'email': req.user.email
                }, function (err, customer) {
                    if (err)
                        throw err;

                    var payload = { id: customer.id, name: customer.name, email: customer.email };
                    var token = jwt.sign(payload, jwtOptions.secretOrKey);

                    res.json({
                        success: true,
                        token: token,
                        user: customer.firstname + ' '+ customer.lastname,
                        id: customer.id,
                        email: customer.email,
                        role: customer.role
                    });
                });
            }
        });
    },
    currentUser: function (req, res) {
        res.status(200).send(req.user);
    },
    register: function (req, res) {
        // Get information on customer
        var firstname = req.body.firstname;
        var lastname = req.body.lastname
        var email = req.body.email;
        var password = req.body.password;
        var telephone = req.body.telephone;


        // Form Validator
        req.checkBody('firstname', 'First name field is required').notEmpty();
        req.checkBody('lastname', 'Last name field is required').notEmpty();
        req.checkBody('email', 'Email field is required').isEmail();
        req.checkBody('password', 'Password field is required').notEmpty();
        req.checkBody('telephone', 'Telephone field is required').notEmpty();

        // Check Errors
        var errors = req.validationErrors();

        if (errors) {
            res.json({ success: false, message: errors[0].msg });
        } else {
            // Create new user 
            var customer = new Customer({
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password,
                mobileNumber: telephone,
                createdDate: Date.now()
            });

            Customer.find({
                'email': email
            }, function (err, user) {
                if (err)
                    throw err;

                if (user.length) {
                    res.json({
                        success: false,
                        message: "User with email already exist"
                    })
                }
                else {
                    Customer.createCustomer(customer, function (err, newCustomer) {
                        if (err)
                            throw err;

                        var cust = Customer.findOne({
                            'email': email
                        }, function (err, customer) {
                            if (err)
                                throw err;

                            var payload = { id: customer.id, name: customer.name, email: customer.email };
                            var token = jwt.sign(payload, jwtOptions.secretOrKey);

                            res.json({
                                success: true,
                                message: 'Customer created!',
                                token: token,
                                user: customer.firstname + ' '+ customer.lastname,
                                id: customer.id,
                                email: customer.email
                            });
                        });
                    });
                }
            });
        }
    },
    retrieveByEmail: function (req, res) {
        Customer.findOne({ 'info.email': req.params.email }, function (err, customer) {
            if (err)
                throw err;

            res.json(customer);
        });
    },
    update: function (req, res) {
        // use our customer model to find the bear we want
        Customer.findOne({ 'info.mobileNumber': req.params.mobileNumber }, function (err, customer) {

            if (err)
                throw err;

            if (req.body.firstname)
                customer.firstname = req.body.firstname;

            if (req.body.lastname)
                customer.lastname = req.body.lastname;

            if (req.body.email)
                customer.email = req.body.email;

            customer.modifiedDate = Date.now();

            // save the customer updateCustomer
            customer.save(function (err) {
                if (err)
                    throw err;

                res.json({ success: true, message: 'Customer updated!' });
            });

        });
    }
}
