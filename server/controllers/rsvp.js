const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');

const RSVP = require('../models/rsvp');
const Orders = require('../models/orders');
//const emailer = require('../../config/email');

// Coupin App Messages
//const messages = require('../../config/messages');

const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey : 'appcustomer'
}

module.exports = {
    getAll: function (req, res) {
        RSVP.getRSVPByOrderId(req.orderId, function (err, orders) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(orders);
            }
        });
    },
    getOne: function(req, res) {
        RSVP.getRSVPByOrderId(req.query.id, function(err, order) {
            if (err)
            throw(err);

            res.json(order);
        })
    },
    register: function (req, res) {
        // Get order details
        const hashtag = req.body.hashtag;
        const guestname = req.body.guestname;
        const response = req.body.response;


        // Form Validator
        req.checkBody('hashtag','Hashtag  field is required').notEmpty();
        req.checkBody('guestname','Guest Name field is required').notEmpty();
        req.checkBody('response', 'Response field cannot be empty').notEmpty();

        // Check Errors
        const errors = req.validationErrors();

        if(errors) {
            res.status(400).send({success: false, message: errors[0].msg });
        } else {
            var createdDate = Date.now();
            var rsvp = new RSVP();      // create a new instance of the Customer model

            Orders.getOrderByName(hashtag, function (err, orders) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    rsvp.orderId = orders.id;
                    rsvp.guestname = guestname;
                    rsvp.response = response;
        
                    rsvp.save(function(err) {
                    if(err) {
                        res.status(500).send(err);
                    } else {
                        res.status(200).send({
                            success: true,
                            message: 'Success! Guest has been registered'});
                        };
                    });
                }
            });

          
        }
    }
}