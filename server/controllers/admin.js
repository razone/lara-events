const User = require('./../models/users');
const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'appcustomer'
}
module.exports = {
    activate : function(req, res) {
        User.findById(req.params.id, function(err, user) {
            if(err) 
                throw err;

            if(!user) {
                res.send({success: false, message: 'No Such User Exists'});
            } else {
                user.isActive = true;
                user.save( function(err) {
                    if(err)
                        throw err;

                    res.send({success: true, message: ' was Activated.'});
                });
            }
        });
    },
    addAdmin : function(req, res, next) {
        // Get information on customer
        var firstname = req.body.firstname;
        var lastname = req.body.lastname
        var email = req.body.email;
        var password = req.body.password;


        // Form Validator
        req.checkBody('firstname', 'First name field is required').notEmpty();
        req.checkBody('lastname', 'Last name field is required').notEmpty();
        req.checkBody('email', 'Email field is required').isEmail();
        req.checkBody('password', 'Password field is required').notEmpty();

        // Check for Errors
        var errors = req.validationErrors();

        if(errors) {
            res.json({ errors: errors});
        } else {
              // Create new user 
              var user = new User({
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password,
                createdDate: Date.now(),
                role: 1
            });

            User.find({
                'email': email
            }, function (err, user) {
                if (err)
                    throw err;

                if (user.length) {
                    res.json({
                        success: false,
                        message: "User with email already exist"
                    })
                }
                else {
                    User.createCustomer(user, function (err, newCustomer) {
                        if (err)
                            throw err;

                        var cust = User.findOne({
                            'email': email
                        }, function (err, user) {
                            if (err)
                                throw err;

                            var payload = { id: user.id, name: user.name, email: user.email };
                            var token = jwt.sign(payload, jwtOptions.secretOrKey);

                            res.json({
                                success: true,
                                message: 'Admin User created!',
                                token: token,
                                user: user.firstname + ' '+ user.lastname
                            });
                        });
                    });
                }
            });
        }
    },
    addSuperAdmin : function(req, res) {
        var user = new User();
        user.email = req.body.email;
        user.password = req.body.password;
        user.role = 0;


        User.createCustomer(user, function(err) {
            if(err)
                throw err;

            res.send({message: 'SuperAdmin Created!'});
        });

    },
    deactivate : function(req, res) {
        User.findById(req.params.id, function(err, user) {
            if(err) 
                throw err;

            if(!user) {
                res.send({success: false, message: 'No Such User Exists'});
            } else {
                user.isActive = false;
                user.save( function(err) {
                    if(err)
                        throw err;

                    res.send({success: true, message: ' was Deactivated.'});
                });
            }
        });
    },
    delete : function(req, res) {
        User.findById(req.params.id, function(err, user) {
            if(err)
                throw err;

            if(user) {
                User.remove({
                    _id : req.params.id
                }, function(err, user) {
                    if(err) {
                        throw err;
                    } else {
                        res.send({success : true, message: 'Admin has been deleted'});
                    }
                    
                });
            }
        });
    },
    getAllAdmins : function (req, res) {
        User.find({}, function(err, user) {
            res.json(user);
        });
    },
    login : function (req, res) {
        req.logIn(req.user, function (err, user) {
            if(err)
                throw err;

            res.redirect('/homepage');
        });
    },
    loginPage: function(req, res) {
        if (req.user) {
            res.redirect('/homepage');
        } else {
            // load the index page
            res.sendfile('./public/views/index.html');
        }
    },
    authenticate: function (req, res) {
        var user = req.user;
        var payload = { id: user.id, name: user.name, email: user.email, role: user.role };
        var token = jwt.sign(payload, jwtOptions.secretOrKey);

        //var token = jwt.sign(customer, secretKey, {
        //  expiresInMinutes: 1440
        //});

        res.json({
            success: true,
            token: 'JWT ' + token
        });
    },
}