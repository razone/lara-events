const express = require('express');
const expressValidator = require('express-validator');
//const emailer = require('../../config/email');
const router = express.Router();
const passport = require('./../middleware/passport');

// Middleware
const auth = require('./../middleware/auth');

// models and controllers
const Orders = require('../models/orders');
const OrderCtrl = require('./../controllers/orders');


// Get all merchants
// router.route('/all')
//   .get(MerchantCtrl.getAllMerchants);

// // Signing in for a merchant
// router.route('/authenticate')
//   .get(auth.authenticate, MerchantCtrl.currentUser)
//   .post(passport.verify, MerchantCtrl.authenticate);

// router.route('/:id')
//   .put(auth.isMerchant, MerchantCtrl.update);

// To call the completion
// router.route('/:id/confirm')
//   .get(MerchantCtrl.getConfirmationPage)
//   // Completion of merchant registration
//   .post(MerchantCtrl.confirm)
//   // For when the admin approves
//   .put(MerchantCtrl.adminReview);

// Querying by Id
 router.route('/latest')
   .get(passport.verifyJWT1, OrderCtrl.getLatestOrder)
   //.delete(MerchantCtrl.deleteOne);

  router.route('/all')
  .get(passport.verifyJWT1, OrderCtrl.getAllOrders)
  //.delete(MerchantCtrl.deleteOne);

  router.route('/all/currentorders')
  .get(passport.verifyJWT1, OrderCtrl.getCurrentOrders)
  //.delete(MerchantCtrl.deleteOne);
  router.route('/all/pages')
  .get(passport.verifyJWT, OrderCtrl.getOrders)
  //.delete(MerchantCtrl.deleteOne);

  router.route('/all/pastorders')
  .get(passport.verifyJWT1, OrderCtrl.getPastOrders)
  //.delete(MerchantCtrl.deleteOne);

// For Registration of merchants
router.route('/register')
  .post(OrderCtrl.register)
  //.get(MerchantCtrl.getRegPage);

  router.route('/update')
  .post(passport.verifyJWT, OrderCtrl.update)

  //Get count for Orders data
  router.route('/info')
  .get(passport.verifyJWT, OrderCtrl.getOrdersData)

module.exports = router;

