const express = require('express');
const expressValidator = require('express-validator');
//const emailer = require('../../config/email');
const router = express.Router();
const passport = require('./../../middleware/passport');

// Middleware
const auth = require('./../../middleware/auth');

// models and controllers
const RSVP = require('./../../models/rsvp');
const RSVPCtrl = require('./../../controllers/rsvp');
const Config = require('./../../models/applicationConfig')
const ConfigCtrl = require('./../../controllers/api/general')

const APICtrl = require('./../../controllers/api/orders');
const Orders = require('./../../models/orders');
const OrderCtrl = require('./../../controllers/orders');

// For Registration of rsvp
router.route('/rsvp/register')
  .post(passport.verifyJWT, RSVPCtrl.register)
  //.get(MerchantCtrl.getRegPage);

  router.route('/orders/directions')
  .get(passport.verifyJWT, APICtrl.getOrderDirections)
  //.delete(MerchantCtrl.deleteOne);

  router.route('/orders/intro')
  .get(passport.verifyJWT, APICtrl.getOrderIntro)
  
  router.route('/config')
  .get(ConfigCtrl.getAll)

  //get user
  router.route('/user')
  .get(passport.verifyJWT, APICtrl.getUser)
  
module.exports = router;

