var express = require('express');
var router = express.Router();
var passport = require('./../middleware/passport');
var jwt = require('jsonwebtoken');
var passportJWT = require("passport-jwt");

var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

//middleware
const auth = require('./../middleware/auth');
// models
const Customer = require('./../models/users');
const CustomerCtrl = require('./../controllers/customer');

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'appcustomer';

router.route('/authenticate')
.post(passport.verify, CustomerCtrl.authenticate)
// To authenticate token
.get(auth.authenticate, CustomerCtrl.currentUser);


router.route('/register')

// register new user
.post(CustomerCtrl.register);

// Get customer by email
router.route('/:email')
.get(CustomerCtrl.retrieveByEmail)

// Used to edit the customer
.put(CustomerCtrl.update);

module.exports = router;
