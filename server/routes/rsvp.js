const express = require('express');
const expressValidator = require('express-validator');
//const emailer = require('../../config/email');
const router = express.Router();
const passport = require('./../middleware/passport');

// Middleware
const auth = require('./../middleware/auth');

// models and controllers
const RSVP = require('../models/rsvp');
const RSVPCtrl = require('./../controllers/rsvp');


// Get all merchants
// router.route('/all')
//   .get(MerchantCtrl.getAllMerchants);

  router.route('/orders/all')
  .get(passport.verifyJWT1, RSVPCtrl.getAll)
  //.delete(MerchantCtrl.deleteOne);


// For Registration of merchants
router.route('/register')
  .post(passport.verifyJWT, RSVPCtrl.register)
  //.get(MerchantCtrl.getRegPage);

module.exports = router;

