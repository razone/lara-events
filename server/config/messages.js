module.exports = {
    approved: function(id) {
        return '<h4> We are excited to have you as part of the team</h4><br/>'
        + 'Please click on the link <a href="http://localhost:5030/merchant/confirm/'+id+'">http://localhost:5030/merchant/confirm/'+id+'</a> to activate your account<br/><br/>'
        + 'If you have any questions, please feel free to message us at info@coupinapp.com<br/><br/>'
        + 'Best Regards,</br>Lara Events.';
    } 
};