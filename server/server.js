const bodyParser = require('body-parser');
// server module
const express = require('express');
//var session = require('express-session');
const app = express();
const methodOverride = require('method-override');

// Express validatory
const expressValidator = require('express-validator');
// Database module
const mongoose = require('mongoose').set('debug',true);
// authentication module
const passport = require('passport');
// session module
const session = require('express-session');
const cookieParser = require('cookie-parser');
// For logging all request
const morgan = require('morgan');
// For token validation
const jwt = require('jsonwebtoken');
const passportJWT = require("passport-jwt");
const fs = require('fs-extra');
const busboy = require('connect-busboy');
//const cloudinary = require('cloudinary');


var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

//var port = process.env.PORT || 5030;
var LocalStrategy = require('passport-local').Strategy;

// Configuration
var db = require('./config/db');
var config = require('./config/env');


const http = require('http');
const serverPort = require ('./config/env');
const path = require('path');
var port = process.env.PORT || 3000;
//app.set("port", port);

/**
 * Create HTTP server.
 */
app.use(express.static(__dirname + '/../dist'));

//  app.get('/*', function(req, res) {
//   res.sendFile(path.join(__dirname + '/../dist/index.html'));
// });
const server = http.createServer(app);

const forceSSL = function() {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(
       ['https://', req.get('Host'), req.url].join('')
      );
    }
    next();
  }
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {

  const normalizedPort = parseInt(val, 10);

  if (isNaN(normalizedPort)) {
    // named pipe
    return val;
  }

  if (normalizedPort >= 0) {
    // port number
    return normalizedPort;
  }

  return false;
}

/**
 * Event listener for HTTP server 'error' event.
 */

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string"
    ? "Pipe " + port
    : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server 'listening' event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === "string"
    ? "pipe " + addr
    : "port " + addr.port;
}
// set our port
//var port = process.env.PORT || 5030;

// connect to db
mongoose.connect(db.url);

/**
 * get all data of the body parameters
 * parse application/json
 */
app.use(busboy());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(morgan('dev'));
// Allow data in url encoded format
app.use(bodyParser.urlencoded({ extended: true }));

// Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// app.use(expressValidator());
// parse application/vnd.api+json as json

/**
 * override with the X-HTTP-Override header in the request.
 * Simulate DEvarE and PUT
 */
app.use(methodOverride('X-HTTP-Method-Override'));

// Add express validator
app.use(expressValidator());

// required for passport to handle sessions
app.use(session({secret: config.secret,
    resave: true,
    saveUninitialized: true}));

// Initialize passport and it's sessions
app.use(passport.initialize());
app.use(passport.session());


/**
 * Listen on provided port, on all network interfaces.
 */
app.set('port', (process.env.PORT || 4200));


// set the static files location /public/img will be /img
//app.use(express.static(__dirname + '/public'));

// Cloudinary config
// cloudinary.config({
//   cloud_name: 'mybookingngtest',
//   api_key: '254821729494622',
//   api_secret: 'F4SmP0wD7kQonfuybQjixWFYzP0'
// });


// app.use('/admin', function (req, res) {
//   res.sendfile('public/views/index.html');
// });

app.post('/upload', function (req, res) {
  cloudinary.uploader.upload(req.body.file, function (result) {
    res.status(200).send(result);
  });
});

//For avoidong Heroku $PORT error
app.get('/', function(request, response) {
    var result = 'App is running'
    response.send(result);
}).listen(app.get('port'), function() {
    console.log('App is running, server is listening on port ', app.get('port'));
});
// configure our routes
require('./routes')(app);

//start on localhost 3030
// app.listen(port).on('error', function (err) {
//   console.log(err);
// });

// confirmation
console.log('Too Cool for port ' +  app.get('port'));

// expose app
exports = module.exports = app;