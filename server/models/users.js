// module
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// define the user schema
var schema = mongoose.Schema;

/**
 * Roles
 * ------
 * 0 - superadmin, 1 - admin, 2 - customer
 */

var userSchema = new schema({

      firstname: {
          type: String,
          lowercase: true
      }, 
      lastname: {
          type: String,
          lowercase: true
      },
      email: {
        type: String,
        lowercase: true,
        required: true,
        unique: true
      },
      mobileNumber: {
          type: String,
          index: true
      },
      password: {
          type: String
      },
      role: {
          type: Number,
          default: 2
      },
      isActive: {
          type: Boolean,
          default: true
      },
      activated: {
            type: Boolean,
            default: false
      },
      createdDate: {
        type: Date,
        default: new Date()
      },
      modifiedDate: {
        type: Date
      }

});

// module.exports allows is to pass this to other files when it is called
const User = mongoose.model('User', userSchema);

User.getCustomerById = function(id, callback) {
	User.findById(id, callback);
};

User.getCustomerByNumber = function(mobileNumber, callback) {
  var query = {mobileNumber: mobileNumber};
	User.findOne(query, callback);
};

User.getCustomerByEmail = function(email, callback) {
	var query = {email: email};
	User.findOne(query, callback);
};

User.comparePassword = function (candidatePassword, hash, callback) {
	bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    	callback(null, isMatch);
	});
};

userSchema.methods.validatePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        callback(null, isMatch);
    });
};

User.validPassword = function( pwd ) {
    // EXAMPLE CODE!
    return ( this.password === pwd );
};

User.createCustomer = function (newCustomer, callback) {
	bcrypt.genSalt(10, function(err, salt) {
    	bcrypt.hash(newCustomer.password, salt, function(err, hash) {
   			newCustomer.password = hash;
   			newCustomer.save(callback);
    	});
	});
};

User.updatePassword = function (user, password, callback) {
    bcrypt.genSalt(10, function(err, salt) {
    	bcrypt.hash(password, salt, function(err, hash) {
   			user.password = hash;
   			user.save(callback);
    	});
	});
};

User.isValid = function (plainPassword, hashedPassword) {
    const salt = bcrypt.genSalt(10);
    return bcrypt.compareSync(plainPassword, hashedPassword);
};

module.exports = User;