const mongoose = require('mongoose');
const schema = mongoose.Schema;
  
let rsvpSchema = new schema({
    orderId: {
        type: String,
        required: true
    },
    guestname: {
        type: String,
        required: true
    },
    response: {
        type: Boolean,
        default: false
    },
    createdDate: {
        type: Date
    }
});

let RSVP = mongoose.model('RSVP', rsvpSchema);

RSVP.saveRSVP = function (newRSVP, callback) {
   			newRSVP.save(callback);
};

RSVP.getRSVPByOrderId = function(orderid, callback) {
		var query = {orderId: orderid};
        RSVP.find(query, callback);
};

module.exports = RSVP;