const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const schema = mongoose.Schema;

let ordersSchema = new schema({
    userId: {
        type: String,
    },
    eventname: {
        type: String,
        unique: true
    },
    category: {
        type: String,
    },
    type: {
        type: String,
    },
    typeUrl: {
        type: String,
    },
    host: {
        type: String,
    },
    callToAction: {
        type: String,
    },
    callToActionDetail: {
        type: String
    },
    dates: [{
        label: {
            type: String
        },
        date: {
            type: Date,
        },
        rsvpdate: {
            type: Date
        }
    }],
    addresses: [{
        address: {
            type: String,
        },
        addressLatitude: {
            type: String
        },
        addressLongitude: {
            type: String
        }
    }],
    status: {
        type: String,
        enum: ['Processing', 'Review', 'Done'],
        default: 'Processing'
    },
    banner: {
        type: String
    },
    videoUrl: {
        type: String
    },
    ctaUrl: {
      type: String
    },
    createdDate: {
        type: Date
    },
    modifiedDate: {
        type: Date
    },
    mobilzationFee: {
        type: Boolean,
        default: false
    },
    deletedOrder: {
      type: Boolean,
      default: false
    },
    finalDeliveryFee: {
        type: Boolean,
        default: false
    },
    optionalAddon: {
        type: Boolean,
        default: false
    }
});

ordersSchema.plugin(mongoosePaginate);

let Orders = mongoose.model('Orders', ordersSchema);

Orders.saveOrder = function (newOrder, callback) {
    newOrder.save(callback);
};

Orders.getOrderByUserId = function (userid, callback) {
    var query = { userId: userid, deletedOrder: false };
    Orders.find(query, callback);
};

Orders.getOrder = function (callback) {
    Orders.find({deletedOrder: false},callback);
};

Orders.getCurrentOrderByUserId = function (userid, callback) {
    var cutoff = new Date();
    var query = { userId: userid, dates:{$elemMatch:{date: { $gte: cutoff }}}, deletedOrder: false };
    Orders.find(query, callback);
};

Orders.getOrderByName = function (eventname, callback) {
    var query = { eventname: eventname, deletedOrder: false };
    Orders.findOne(query, callback);
};

Orders.getPastOrderByUserId = function (userid, callback) {
    var cutoff = new Date();
    var query = { userId: userid, dates:{$elemMatch:{date: { $lt: cutoff }}}, deletedOrder: false };
    Orders.find(query, callback);
};

Orders.getLatestOrder = function (userid, callback) {
    var query = { userId: userid, deletedOrder: false };
    Orders.findOne(query, callback).sort({ createdDate: 'desc' }).exec(function (err, docs) { });
};

module.exports = Orders;
