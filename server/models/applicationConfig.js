// module
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// define the config schema
var schema = mongoose.Schema;
var configSchema = new schema(schema.Types.Mixed, { strict: false, collection: 'applicationconfig'});


// module.exports allows is to pass this to other files when it is called
const Config = mongoose.model('applicationconfig', configSchema);


module.exports = Config;